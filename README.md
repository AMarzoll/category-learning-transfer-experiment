# Overview

This project contains the Matlab files for the category transfer experiment. Visual stimuli need to be categorized according to four feature dimensions (linear grating orientation and spatial frequency, circular grating spatial frequency and Gaussian envelope notch depth). Stimulus features and category boundary differ between subjects and two experimental phases for each subject. Furthermore, an fine orientation discrimination task is contained in each session.

## Features

- Stimulus and categorization properties can be configured with an external file, the contents of which are garbled to prevent subjects from snooping.
- During the first session, physical screen size (credit card method) and subject's blind spot will be measured to calculate distance to the screen. This corresponds to a "virtual chin-rest" (Li et al., 2020, Scientific Reports 10:904).
- Incomplete sessions can be easily resumed from autosaved data using the UI without experimenter intervention.
- Upon completion, experimental data can be sent back automatically to the experimenter via email.
- Experiment enters pause mode if subject ceases to respond. From that mode, subjects can choose to complete the session at a later time using the GUI.

## Structure

- A Matlab GUI application (`ExperimentLauncher.mlapp`) is wrapped around the Psychophysics-toolbox entry-point and main loop of the program (`main.m`).
- Abstract classes for the experiment loop itself and the staircase procedure are provided (`ExperimentLoop.m` and `Staircase.m`), which are used for both the categorization and the orientation discrimination task.

## Requirements

This program requires GStreamer and Psychtoolbox-3 to run (https://psychtoolbox.org/) and the Matlab application compiler for standalone deployment. OS-specific files listed below:
- MacOS:
    - macOS 10.11 or later: https://gstreamer.freedesktop.org/data/pkg/osx/1.18.2/gstreamer-1.0-1.18.2-x86_64.pkg
    - macOS 10.10: https://gstreamer.freedesktop.org/data/pkg/osx/1.16.3/gstreamer-1.0-1.16.3-x86_64.pkg
- Windows:
    - https://gstreamer.freedesktop.org/data/pkg/windows/1.18.2/msvc/gstreamer-1.0-msvc-x86_64-1.18.2.msi
    - https://www.microsoft.com/en-au/download/confirmation.aspx?id=48145

## How to use

The program needs to be compiled for the target OS by using the Matlab application compiler (`ExperimentLauncher.prj`). The application compiler will find most of the necessary files to be packaged, but the following directories from Psychtoolbox will have to be added manually:

- PsychOpenGL folder
- PsychBasic/Plugins folder
- PsychContributed folder

After compilation, an installer file will be placed in the ExperimentLauncher/for_redistribution directory that can be made available to participants. Individualized experimental parameters are expected in a folder named `visual_experiment` within the participant's home folder (i.e., Windows: `%USERPROFILE%\visual_experiment`; MacOS: `/Users/<username>/visual_experiment` (can also be reached by Command+Shift+H)). On more recent MacOS versions, the installed program requires explicit access to keyboard input, which can be enabled in the OS options. Before the first session, ask the subject to have a credit-card sized object at hand for the screen size measurement. A file named exp.cfg describes the parameters of the categorization and orientation discrimination tasks and can be acquired from me (one parameter per line, in the format `key   value`, i.e. separated by a tab). It is populated with a number of variables (see below) and then obfuscated using the static method `CfgFileUtils.convertConfigFile(textOrFileToEncode, outputFilePath)`. If necessary, the obfuscation can be reversed using `CfgFileUtils.decodeConfigFile(textFileToDecode, outputFilePath)`. 

- Required parameters in exp.cfg:
    - subj.ID
    - currentPhase
    - exp.cat.catCenter1
    - exp.cat.catCenter2
    - exp.cat.catCenter3
    - exp.cat.catCenter4
    - exp.cat.catRadius
    - exp.cat.catBoundaryNormVec1_1
    - exp.cat.catBoundaryNormVec1_2
    - exp.cat.catBoundaryNormVec1_3
    - exp.cat.catBoundaryNormVec1_4
    - exp.cat.catBoundaryNormVec2_1
    - exp.cat.catBoundaryNormVec2_2
    - exp.cat.catBoundaryNormVec2_3
    - exp.cat.catBoundaryNormVec2_4
    - exp.od.firstOri
    - experimenterMail
    - botMail
    - botPhrase
    - botHostSmtp
    - botHostPort

## Troubleshooting

- Occasionally, the dot during the blind spot task or the font in the experiment are invisible or difficult to see. In that case, click the cat image on the first GUI screen ten times to reveal the debug menu. Choose a different dot shader or font renderer and it should work.