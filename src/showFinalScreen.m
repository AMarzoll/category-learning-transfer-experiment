function showFinalScreen(winPtr, dis, machine, hasMailFailed)
%Notify the subject that the experiment has ended.

    notificationStr = ['-- EXPERIMENT SESSION FINISHED --\n\nThank you for your participation today. Please contact the experimenter for ', ...
                       'another session if applicable.'];
    failStr         = ['\n\n--AUTOMATIC RESULT SUBMISSION FAILED--\n\nSending results via internet connection has failed. Kindly contact ' ...
                       'the experimenter for further instructions.'];
    endStr          =  '\n\nPress any key to exit.';
           
    if hasMailFailed
        str = strcat(notificationStr, failStr, endStr);
    else
        str = strcat(notificationStr, endStr);
    end
    
    [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, str, dis.screenRect, 300, 100, true);
    Screen('TextSize', winPtr, newFontSize);
    DrawFormattedText(winPtr, str, 'center', 'center', dis.textColor);
    Screen('Flip', winPtr);
    Screen('TextSize', winPtr, oldFontSize);
    WaitSecs(1.5);
    KbStrokeWait(machine.kbIdx);
end