function blindspotOffsets_pix = blindSpotDistanceEstimation(winPtr, kbIdx, dotsRenderMode)
%This function enables to indirectly measure subjects' screen distance by
%measuring the location of their blind spot (see Li et al., 2020, SciRep).
%The return value is the offset of the participant's blind spot in pixels
%(you need the pixel-per-cm value from the 'pixPerCmEstimation' function to
%convert this value to cm).
%It requires a pointer to a Psychtoolbox window and the index of the
%keyboard that's supposed to be used. This function depends on the 'Button'
%class and the 'adaptTextBoundsToRect' function to work. Apart from that,
%it can be plugged into your experiment easily.

%parameter configuration
nTrials                  = 5;
nFailuresToReset         = 5;
deviationTolerancePerc   = 0.05; %deviation from the mean after n trials that trials are allowed before they are invalidated/repeated

fixCrossSize_norm        = 0.015; %normalized parameters as percentage of entire screen height/width
fixCrossOffset_norm      = 0.25; %offset from the center to the right
movDotSize_norm          = 0.005;
movDotSpeed_normPerSec   = 0.04;
movDotOffset_norm        = 0.1;
resetButtonCenter_norm   = [0.85, 0.30];
backButtonCenter_norm    = [0.85, 0.70];

fixCrossColor            = [1.00, 1.00, 1.00];
movDotColor              = [1.00, 0.00, 0.00];
buttonColor              = [0.01 0.01 0.01];
buttonTextColor          = [0.60 0.60 0.60];

%strings
instructionStr = ['-- BLIND SPOT ESTIMATION --\nINSTRUCTIONS\n\nIn the following task, we need to locate the position of your ''blind spot''. The blind ' ...
                  'spot is the location where the optic nerve exits the eye. In this spot, there are no light-sensitive cells and ' ...
                  'you are unable to see. Because most people have two eyes and the brain usually ''fills in'' the gap, you are ' ...
                  'unaware of this blind spot in everyday life.\n\nPlease assume the position you intend to keep throughout the' ...
                  'remainder of the experiment. Completely cover your _right_ eye with your hand. On the next screen, there will ' ...
                  'be a fixation cross to the right of the center. Please keep your left eye fixated on this cross at all times. To the left ' ...
                  'of the cross, a red dot will start to move further to the left. At some point, the red dot will enter your left ' ...
                  'eye''s blind spot and apparently vanish. When this happens, please press the space bar as quickly as possible. ' ...
                  'Please try to resist the urge to press the key prematurely or to follow the red dot with your eye. If you do not ' ...
                  'see the dot vanish before it leaves the left edge of your screen (indicated by a sound), you need to move closer ' ...
                  'to your screen. In that case, reset the measurement with the button on the right. This procedure will be repeated ' ...
                  'a number of times.\n\nWhen you are ready, press any key to continue.'];
resetButtonStr =  'Reset Measurements';
backButtonStr  =  'Back to Instructions';

%get some screen info
[x_pix, y_pix] = Screen('WindowSize', winPtr);
ctrX_pix = x_pix/2;
ctrY_pix = y_pix/2;
halfFrameDur_sec = 0.5 * (1/FrameRate(winPtr));

%calculate values for on-screen presentation
fixCrossSize_pix      = fixCrossSize_norm * x_pix;
fixCrossSizeX_pix     = [ctrX_pix - fixCrossSize_pix/2, ctrX_pix + fixCrossSize_pix/2];
fixCrossSizeY_pix     = [ctrY_pix - fixCrossSize_pix/2, ctrY_pix + fixCrossSize_pix/2];
fixCrossOffset_pix    = fixCrossOffset_norm * x_pix;
movDotSize_pix        = movDotSize_norm * x_pix;
movDotSpeed_pixPerSec = movDotSpeed_normPerSec * x_pix;
movDotOffset_pix      = movDotOffset_norm * x_pix;
movDotStartPos_pix    = [(ctrX_pix + fixCrossOffset_pix) - movDotOffset_pix, ctrY_pix];
movDotCurPos_pix      = movDotStartPos_pix;

%create buttons
resetButton = Button(resetButtonCenter_norm, resetButtonStr, [0 0 x_pix y_pix]);
resetButton = resetButton.setButtonColor(buttonColor);
resetButton = resetButton.setTextColor(buttonTextColor);
backButton  = Button(backButtonCenter_norm, backButtonStr, [0 0 x_pix y_pix]);
backButton  = backButton.setButtonColor(buttonColor);
backButton  = backButton.setTextColor(buttonTextColor);

%presentation preparation
ShowCursor(1);
spacebarKeycode = KbName('space');
vbl = GetSecs(); % = vertical blank
blindspotOffsets_pix = nan(1, nTrials);
measIdx = 1;
debounce = false;
failureCounter = 0;
showInstructions = true;
resetFlag = false;

%do task until five valid responses are recorded
while true
    
    %present instructions
    if showInstructions
        [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, instructionStr, [0 0 x_pix y_pix], 100, 100, true);
        Screen('TextSize', winPtr, newFontSize);
        DrawFormattedText(winPtr, instructionStr, 'center', 'center', [0 0 0], 80, false, false, 1.5);
        vbl = Screen('Flip', winPtr);
        Screen('TextSize', winPtr, oldFontSize);
        showInstructions = false;
        WaitSecs(0.5);
        KbStrokeWait(kbIdx);

        %countdown
        for i = [3:-1:1]
            DrawFormattedText(winPtr, num2str(i), 'center', 'center', [0 0 0]);
            vbl = Screen('Flip', winPtr, vbl + 1.0 - halfFrameDur_sec);
        end
        WaitSecs(1.0);
    end
    
    Screen('FillRect', winPtr, [0 0 0], [0 0 x_pix y_pix]);
    
    %handle reset button
    isResetButtonPressed = resetButton.drawButton(winPtr);
    if isResetButtonPressed
        resetFlag = true;
    end
    
    %handle back button
    isBackButtonPressed = backButton.drawButton(winPtr);
    if isBackButtonPressed
        showInstructions = true;
        resetFlag = true;
    end
    
    %draw fixation cross
    Screen('DrawLine', winPtr, fixCrossColor, fixCrossOffset_pix + fixCrossSizeX_pix(1), ...
                                              ctrY_pix, ...
                                              fixCrossOffset_pix + fixCrossSizeX_pix(2), ...
                                              ctrY_pix);
    Screen('DrawLine', winPtr, fixCrossColor, fixCrossOffset_pix + ctrX_pix, ...
                                              fixCrossSizeY_pix(1), ...
                                              fixCrossOffset_pix + ctrX_pix, ...
                                              fixCrossSizeY_pix(2));
    
    %move dot and draw it
    movDotCurPos_pix(1) = movDotCurPos_pix(1) - movDotSpeed_pixPerSec*2*halfFrameDur_sec;
    if movDotCurPos_pix(1) < 0
        movDotCurPos_pix(1) = movDotStartPos_pix(1);
        Beeper();
    end
    Screen('DrawDots', winPtr, movDotCurPos_pix, movDotSize_pix, movDotColor, [], dotsRenderMode);
    
    %render everything
    vbl = Screen('Flip', winPtr, vbl+halfFrameDur_sec);
    
    %get button press
    [isKeyPressed, ~, keyCodes] = KbCheck(kbIdx);
    if isKeyPressed && keyCodes(spacebarKeycode) && ~debounce
        blindspotOffsets_pix(measIdx) = round((fixCrossOffset_pix + ctrX_pix) - movDotCurPos_pix(1));
        measIdx = measIdx + 1;
        debounce = true; %ensure a button press is recorded as only a single response
        movDotCurPos_pix(1) = movDotStartPos_pix(1); %reset dot
    elseif ~isKeyPressed
        debounce = false;
    end
    
    %check validity of measurements
    if measIdx > nTrials
        invalidMeasures = abs(median(blindspotOffsets_pix) - blindspotOffsets_pix) > median(blindspotOffsets_pix)*deviationTolerancePerc;
        if any(invalidMeasures)
            blindspotOffsets_pix(invalidMeasures) = [];
            measIdx = numel(blindspotOffsets_pix)+1;
            failureCounter = failureCounter + 1;
            if failureCounter >= nFailuresToReset
                resetFlag = true;
            end
        else
            break; %measurements complete
        end
    end
    
    %reset measurement
    if resetFlag %when things go horribly wrong, repeat entire measurement series
        resetFlag = false;
        blindspotOffsets_pix = nan(1, nTrials);
        movDotCurPos_pix(1) = movDotStartPos_pix(1);
        measIdx = 1;
        failureCounter = 0;
    end
end
HideCursor();
end