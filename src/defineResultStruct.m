function res = defineResultStruct(exp, catTaskFlag, odTaskFlag)
%Pre-populate the result struct named 'res'. Also insert some labels to be
%able to tell what's what.

    %% categorization task
    if catTaskFlag
        %first dim: block; second dim: trial, third dim:
            %1: trial#, 2: category, 3: distance from boundary, 4,5,6,7: normalized stimulus
            %parameters, 8,9,10,11: stimulus parameters, 12: response, 13: hit, 14: missed trial (response too late), 15: response time
        res.cat.trialTable = nan(exp.cat.nBlocks, exp.cat.nTrialsPerBlock, 15);
        propNames = CatStimulus.getStimPropertyNames();
        res.cat.trialTableLabels = ["trial", "category", "boundary_dist", propNames(1)+"_norm", propNames(2)+"_norm", propNames(3)+"_norm", ...
                                propNames(4)+"_norm", propNames(1), propNames(2), propNames(3), propNames(4), "response", "hit", ...
                                "missed_trial", "reponse_time"];

        %first dim: block, second dim:
            %1: block#, 2: mean boundary distance, 3: threshold value,
            %4: number of missed trials, 5: mean response time, 6: response
            %bias
        res.cat.blockTable = nan(exp.cat.nBlocks, 6);
        res.cat.blockTableLabels = ["block", "avg_boundary_dist", "threshold", "missed_trials", "avg_response_time", "response_bias"];

        res.cat.nReversals = 6; %number of reversals for on-the-fly threshold calculation;

        %pre-allocate
        res.cat.stimuli(1:exp.cat.nBlocks, 1:exp.cat.nTrialsPerBlock) = deal(CatStimulus([], []));

        %pre-allocate flip timing info matrices
        res.cat.sot = nan(exp.cat.nBlocks, exp.cat.nTrialsPerBlock, CatExperiment.nFlipsPerTrial);
        res.cat.ft  = nan(exp.cat.nBlocks, exp.cat.nTrialsPerBlock, CatExperiment.nFlipsPerTrial);
        res.cat.ms  = nan(exp.cat.nBlocks, exp.cat.nTrialsPerBlock, CatExperiment.nFlipsPerTrial);
    end
    
    %% orientation discrimination task
    if odTaskFlag
        %first dim: block; second dim: trial, third dim:
            %1: trial#, 2: deviation direction (clockwise or counterclockwise),
            %3: deviation angle, 4: response, 5: hit, 6: missed trial (response
            %too late), 7: response time
        res.od.trialTable = nan(2, exp.od.nBlocks, exp.od.nTrialsPerBlock, 8);
        res.od.trialTableLabels = ["trial", "reference_angle", "dev_direction", "dev_angle", "response", "hit", "missed_trial", "reponse_time"];
        %first dim: block, second dim:
            %1: block#, 2: mean absolute deviation angle, 3: threshold value,
            %4: number of missed trials, 5: mean response time, 6: response
            %bias
        res.od.blockTable = nan(2, exp.od.nBlocks, 7);
        res.od.blockTableLabels = ["block", "reference_angle", "avg_dev_angle", "threshold", "missed_trials", "avg_response_time", "response_bias"];

        res.od.nReversals = 6;

        %pre-allocate
        res.od.stimuli(1:2, 1:exp.od.nBlocks, 1:exp.od.nTrialsPerBlock) = deal(OdStimulus([], [], []));
        res.od.staircases(1:2, 1:exp.od.nBlocks) = deal(OdStaircase([], []));
        
        %pre-allocate flip timing info matrices
        res.od.sot = nan(2, exp.od.nBlocks, exp.od.nTrialsPerBlock, OdExperiment.nFlipsPerTrial);
        res.od.ft  = nan(2, exp.od.nBlocks, exp.od.nTrialsPerBlock, OdExperiment.nFlipsPerTrial);
        res.od.ms  = nan(2, exp.od.nBlocks, exp.od.nTrialsPerBlock, OdExperiment.nFlipsPerTrial);
    end
end