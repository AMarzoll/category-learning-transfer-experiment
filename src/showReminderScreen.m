function exitFlag = showReminderScreen(winPtr, dis)
%Notify the subject that the experiment has ended.
    
    [x_pix, y_pix] = Screen('WindowSize', winPtr);

    str = ['-- WELCOME --\n\nHere are some helpful reminders:\n\n-Please make sure that your screen brightness is adjusted to ', ...
           'the level determined in the first session.\nIf this is your first session, please set your screen brightness to a ', ...
           'level of about 50% and adjust it if necessary.\nThe gray background should not cause discomfort on your eyes and ', ...
           'the black-and-white visual stimuli should look ''balanced''.\n\n-Please make sure that your computer screen is the ', ...
           'only major light source in your room to the extent that this is feasible.\n\n-Windows users: Please make sure that scaling ', ...
           'of GUI elements is set to 100%.'];
           
       
    exitButton = Button([0.25, 0.80], 'Quit program', [0 0 x_pix y_pix]);
    contButton = Button([0.75, 0.80], 'Continue', [0 0 x_pix y_pix]);
    
    ShowCursor(1);
       
    while true
        [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, str, dis.screenRect, 400, 100, true);
        Screen('TextSize', winPtr, newFontSize);
        DrawFormattedText(winPtr, str, 'center', 'center', dis.textColor);
        
        isExitButtonPressed = exitButton.drawButton(winPtr);
        isContButtonPressed = contButton.drawButton(winPtr);
        
        if isExitButtonPressed
            exitFlag = true;
            return;
        end
        
        if isContButtonPressed
            exitFlag = false;
            return;
        end
        
        Screen('Flip', winPtr);
        Screen('TextSize', winPtr, oldFontSize);
    end
end

