classdef SaveManager
    %Handles saving to and loading from save files. Also handles the
    %displaying of options when at least one autosave file has been
    %detected, enabling the subject to continue the experiment after the
    %last completed block.
    
    
    properties (SetAccess = private)
        cfgFileUtils
        
        userDir
        expSubDir
        saveFileDir = 'save_files'
        saveFilePath
        fileName
        startTime
        ID %subject ID string
        
        autosaveFileNames
        autosaveFileDatestrings
        
        instructionStr = ['-- SESSION RESTORATION --\nINSTRUCTIONS\nAt least one incomplete experiment session was detected. If your session unexpectedly crashed, ' ...
                          'you can resume it by clicking a button on the right. The label indicates the starting time of the ' ...
                          'session. If you want to start a new session, please click the button below. If you are unsure ' ...
                          'whether you should resume or start a new session, please contact the experimenter immediately.'];
        skipButtonStr  =  'Start New Session';
        exitButtonStr  =  'Quit';
        
        horzCanvasSize_norm = 0.50;
        vertCanvasMargin_norm = 0.05;
        vertButtonDist_norm = 0.05;
        buttonWidth_norm = 0.25;        
    end
    
    
    
    methods
        function obj = SaveManager(machine, subj, cfgFileUtils)
            %find user directory
            if ispc
                obj.userDir = getenv('USERPROFILE');
            else
                obj.userDir = getenv('HOME');
            end
            obj.expSubDir = machine.expSubDir;
            obj.startTime = subj.startTime;
            obj.ID = subj.ID;
            obj.cfgFileUtils = cfgFileUtils;
            timeStr = datestr(obj.startTime, 'yyyy_mm_dd_HH_MM_SS');
            obj.fileName = strcat(obj.ID, '_', timeStr, '.mat');
            obj.saveFilePath = fullfile(obj.userDir, obj.expSubDir, obj.saveFileDir);
            if ~exist(obj.saveFilePath, 'dir')
                mkdir(obj.saveFilePath);
            end
        end
        
        
        
        function createAutosaveFile(obj, machine, subj, dis, exp, res)
            completeFileName = strcat('AUTOSAVE_', obj.fileName);
            completePath = fullfile(obj.saveFilePath, completeFileName);
            save(completePath, 'machine', 'subj', 'dis', 'exp', 'res');
        end
        
        
        
        function deleteAutosaveFile(obj)
            curSaveFile = fullfile(obj.saveFilePath, obj.fileName);
            if exist(curSaveFile, 'file')
                autosaveCompleteFileName = strcat('AUTOSAVE_', obj.fileName);
                autosaveCompletePath = fullfile(obj.saveFilePath, autosaveCompleteFileName);
                delete(autosaveCompletePath);
            end
        end
        
        
        
        function saveFilePath = createSaveFile(obj, machine, subj, dis, exp, res)
            completePath = fullfile(obj.saveFilePath, obj.fileName);
            save(completePath, 'machine', 'subj', 'dis', 'exp', 'res');
            saveFilePath = completePath;
        end
        
        
        
        function [isAutosaveFilePresent, obj] = checkAutosaveFilePresence(obj)
            obj.autosaveFileNames = [];
            obj.autosaveFileDatestrings = [];
            fIdx = 1;
            files = dir(fullfile(obj.saveFilePath, '*.mat'));
            if isempty(files)
                isAutosaveFilePresent = false;
                return;
            end
            for iFile = 1:numel(files)
                if strfind(files(iFile).name, 'AUTOSAVE_')
                    obj.autosaveFileNames{fIdx} = files(iFile).name;
                    load(fullfile(obj.saveFilePath, files(iFile).name), 'subj');
                    if exist('subj', 'var')
                        if isfield(subj, 'startTimeStr')
                            obj.autosaveFileDatestrings{fIdx} = subj.startTimeStr;
                        end
                    end
                    fIdx = fIdx + 1;
                end
            end
            if ~isempty(obj.autosaveFileNames)
                isAutosaveFilePresent = true;
            else
                isAutosaveFilePresent = false;
            end
        end
        
        
        
        function [restoreFlag, restoreFileName, exitFlag] = showAutosaveRestoreScreen(obj, winPtr)
            [x_pix, y_pix] = Screen('WindowSize', winPtr);
            restoreFlag = [];
            restoreFileName = [];
            exitFlag = false;
            
            %convenience lambda
            norm2pixRect = @(in) round([in(1)*x_pix, in(2)*y_pix, in(3)*x_pix, in(4)*y_pix]);
            
            %make button parameters
            nAutosaveButtons = min(numel(obj.autosaveFileNames), 9); %won't fit more than 9 buttons..more are unrealistic in real-world scenarios
            for iButton = 1:nAutosaveButtons
                buttonCenter = [1.5 * obj.horzCanvasSize_norm, ...
                                obj.vertCanvasMargin_norm + (iButton-1)*(Button.defaultButtonHeight_norm + obj.vertButtonDist_norm)];
                saveButtonArray(iButton) = Button(buttonCenter, obj.autosaveFileDatestrings{end-iButton+1}, [0 0 x_pix y_pix]);
                saveButtonArray(iButton) = saveButtonArray(iButton).setButtonWidth(obj.buttonWidth_norm);
            end
            
            skipButtonCenter = [0.5 * (1 - obj.horzCanvasSize_norm), 0.75];
            skipButton = Button(skipButtonCenter, obj.skipButtonStr, [0 0 x_pix y_pix]);
            skipButton = skipButton.setButtonWidth(obj.buttonWidth_norm);
            
            exitButtonCenter = [0.5 * (1 - obj.horzCanvasSize_norm), 0.85];
            exitButton = Button(exitButtonCenter, obj.exitButtonStr, [0 0 x_pix y_pix]);
            exitButton = exitButton.setButtonWidth(obj.buttonWidth_norm);
            
            %make canvas for instruction text
            instructionRect_norm = [0, 0, (1-obj.horzCanvasSize_norm), 0.6];
            instructionRect_pix = norm2pixRect(instructionRect_norm);
            
            ShowCursor(1);
            breakFlag = false;
            instructionFontSize = [];
            instructionOldFontSize = [];
            while true
                %draw instruction text
                if isempty(instructionFontSize)
                    [~, instructionOldFontSize, instructionFontSize] = adaptTextBoundsToRect(winPtr, obj.instructionStr, instructionRect_pix, 50, 50, true);
                end
                Screen('TextSize', winPtr, instructionFontSize);
                DrawFormattedText(winPtr, obj.instructionStr, 'center', 'center', [0 0 0], 80, false, false, 1.5, 0, instructionRect_pix);
                Screen('TextSize', winPtr, instructionOldFontSize);
                
                %draw buttons and handle clicks
                for iButton = 1:nAutosaveButtons
                    isButtonPressed = saveButtonArray(iButton).drawButton(winPtr);
                    if isButtonPressed
                        restoreFlag = true;
                        restoreFileName = obj.autosaveFileNames{end-iButton+1};
                        breakFlag = true;
                    end
                end
                
                %draw skip button
                isSkipButtonPressed = skipButton.drawButton(winPtr);
                if isSkipButtonPressed
                    restoreFlag = false;
                    restoreFileName = [];
                    break;
                end

                %draw exit button
                isExitButtonPressed = exitButton.drawButton(winPtr);
                if isExitButtonPressed
                    exitFlag = true;
                    return;
                end
                
                %render stuff
                Screen('Flip', winPtr);
                
                %break here to avoid graphics buffer leftovers
                if breakFlag
                    break;
                end
            end
            HideCursor();
        end
        
        
        
        function [machine, subj, dis, exp, res, catStartBlock, odStartBlock, odStartOri, catTaskFlag, odTaskFlag] = loadAutosaveFile(obj, restoreFileName)
            catStartBlock = [];
            odStartBlock = [];
            odStartOri = [];
            odTaskFlag = false;
            catTaskFlag = false;
            
            %load file
            restorePath = fullfile(obj.saveFilePath, restoreFileName);
            if ~exist(restorePath, 'file')
                error('Error loading autosave file')
            end
            load(restorePath, 'machine', 'subj', 'dis', 'exp', 'res');
            
            %figure out block in categorization task to start with
            if isfield(exp, 'cat')
                catTaskFlag = true;
                for iBlock = 1:exp.cat.nBlocks
                    %check for the first block with all NaN responses and
                    %return its index as the start block
                    if all(isnan(res.cat.trialTable(iBlock, :, 12)))
                        catStartBlock = iBlock;
                        break;
                    end
                end
            end
            
            %figure out block in orientation discrimination task to start with
            breakFlag = false;
            if isfield(exp, 'od')
                odTaskFlag = true;
                
                if obj.cfgFileUtils.readParam('exp.od.firstOri', 'num') == 1
                    oris = [1 2];
                else
                    oris = [2 1];
                end
                
                for iOri = oris
                    for iBlock = 1:exp.od.nBlocks
                        %check for the first block with all NaN responses and
                        %return its index as the start block
                        if all(isnan(res.od.trialTable(iOri, iBlock, :, 5)))
                            odStartOri = iOri;
                            odStartBlock = iBlock;
                            breakFlag = true;
                        end
                        if breakFlag
                            break;
                        end
                    end
                    if breakFlag
                        break;
                    end
                end
            end
            
            if isempty(catStartBlock) && isfield(exp, 'cat')
                error('No incomplete categorization block found in this autosave file - session already completed?');
            end
            if isempty(odStartBlock) && isfield(exp, 'od')
                error('No incomplete orientation discrimination block found in this autosave file - session already completed?');
            end
        end
        
    end
end