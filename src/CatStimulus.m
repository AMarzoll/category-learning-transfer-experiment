classdef CatStimulus
    %handles categorization task stimulus image generation, the constructor
    %converts four normalized parameters in the range [0, 1] to proper
    %units

    properties (SetAccess = private)
        
        ori        = NaN; %orientation of Gabor grating
        sf1        = NaN; %spatial frequency of Gabor
        sf2        = NaN; %spatial frequency of concentric sine-wave
        sRng       = NaN; %Gaussian mask sigma variability
        
        ori_norm   = NaN;
        sf1_norm   = NaN;
        sf2_norm   = NaN;
        sRng_norm  = NaN;
        
        sf1_pix    = NaN;
        sf2_pix    = NaN;
        sMin_pix   = NaN;
        sMax_pix   = NaN;
        
        phase_lin  = NaN; %phase of gratings
        phase_con  = NaN;
        
        sz_pix     = NaN;
        
    end
    
    properties (Constant)
        
        ori_scale  = 'linear'; %determines if normalized values map linearly or logarithmically on actual parameter values
        ori_low    = 7.5; %in degrees
        ori_high   = 82.5;
        
        sf1_scale  = 'log';
        sf1_low    = 1.5; %in cycles per degree
        sf1_high   = 10;
        
        sf2_scale  = 'log';
        sf2_low    = 1.5; %in cycles per degree
        sf2_high   = 10;

        sRng_scale = 'exp_base_0.25';
        sRng_low   = 0.1; %Gaussian mask sigma deviation in degrees of visual angle
        sRng_high  = 0.5;
        
        sz_deg          = 5; %canvas size in degrees of visual angle
        c_lin           = 0.3; %sine-wave contrast of linear component, dimensionless scaling factor
        c_con           = 0.3; %sine-wave contrast of concentric component
        nGaussNotches   = 5; %number of notches within the stimulus
        sMean           = 0.6; %mean Gaussian mask sigma
        c_noise         = 0.6; %contrast for Gaussian noise
        
    end

%%
    methods
        
        function obj = CatStimulus(stimParams_norm, dis)
            %initialize even if parameters are empty (for memory
            %pre-allocation)
            if isempty(stimParams_norm) && isempty(dis)
                return;
            end
            
            obj.ori_norm  = stimParams_norm(1);
            obj.sf1_norm  = stimParams_norm(2);
            obj.sf2_norm  = stimParams_norm(3);
            obj.sRng_norm = stimParams_norm(4);
            
            %convert into proper units
            obj.ori  = CatStimulus.norm2var(stimParams_norm(1), obj.ori_scale, obj.ori_low, obj.ori_high);
            obj.sf1  = CatStimulus.norm2var(stimParams_norm(2), obj.sf1_scale, obj.sf1_low, obj.sf1_high);
            obj.sf2  = CatStimulus.norm2var(stimParams_norm(3), obj.sf2_scale, obj.sf2_low, obj.sf2_high);
            obj.sRng = CatStimulus.norm2var(stimParams_norm(4), obj.sRng_scale, obj.sRng_low, obj.sRng_high);
            
            %convert relevant measures to pixels
            obj.sz_pix  = round(dis.deg2pix(obj.sz_deg));
            obj.sMin_pix = dis.deg2pix(obj.sMean - obj.sRng / 2);
            obj.sMax_pix = dis.deg2pix(obj.sMean + obj.sRng / 2);
            obj.sf1_pix = 1/dis.deg2pix(1/obj.sf1);
            obj.sf2_pix = 1/dis.deg2pix(1/obj.sf2);
            
            %randomize phase of sine waves
            obj.phase_lin = 2.0*pi*rand;
            obj.phase_con = 2.0*pi*rand;
        end
        
        
        
        function img = generateImage(obj)
            
            %generate coordinate systems (Cartesian and polar)
            if mod(obj.sz_pix, 2) == 0
                [x,y] = meshgrid(-obj.sz_pix/2+1:obj.sz_pix/2, obj.sz_pix/2:-1:-obj.sz_pix/2+1);
            else
                [x,y] = meshgrid(-obj.sz_pix/2+0.5:obj.sz_pix/2-0.5, obj.sz_pix/2-0.5:-1:-obj.sz_pix/2+0.5);
            end
            [theta, rho] = cart2pol(x,y);
            
            %actual image (linear sine-wave + concentric sine-wave + Gaussian mask
            img = 127*(1.0 + (obj.c_lin*(sin(2.0*pi*obj.sf1_pix*(y*sind(obj.ori) +  x*cosd(obj.ori)) + obj.phase_lin) ... %Gabor component
                              + obj.c_con*sin(2.0*pi*obj.sf2_pix*rho + obj.phase_con))) ... %concentric component
                       .* exp(-(x.^2.0+y.^2.0)./(2.0*(obj.sMin_pix+(0.5*(sin(obj.nGaussNotches*theta)+1.0)*(obj.sMax_pix-obj.sMin_pix))).^2.0))); %Gaussian mask
            img = img./255;
        end
        
        
        
        function img = generateNoise(obj)
            img = 0.5 + randn(obj.sz_pix, obj.sz_pix) * obj.c_noise;
        end
            
        
        
        function res = getStimProperties(obj)
            res = [obj.ori, obj.sf1, obj.sf2, obj.sRng];
        end
        
    end
    
%%
    methods (Static)
        
        function var = norm2var(varNorm, varScale, varLow, varHigh)
            assert(isa(varNorm, 'float'), 'var_norm must be a floating point type')
            assert((varNorm <= 1.0) && (varNorm >= 0.0), 'var_norm out of range');
            if strcmp(varScale, 'linear')
                var = (varHigh - varLow) * varNorm + varLow;
            elseif strcmp(varScale, 'log')
                if varLow == 0
                    error('When using logarithmic scaling, minimum value must not be zero');
                end
                var = exp((log(varHigh) - log(varLow)) * varNorm + log(varLow));
            elseif strcmp(varScale, 'exp_base_e')
                var = log((exp(varHigh) - exp(varLow)) * varNorm + exp(varLow));
            elseif strcmp(varScale, 'exp_base_10')
                var = log10((10^varHigh - 10^varLow) * varNorm + 10^varLow);
            elseif strcmp(varScale, 'exp_base_2')
                var = log2((2^varHigh - 2^varLow) * varNorm + 2^varLow);
            elseif strcmp(varScale, 'exp_base_0.25')
                base = 0.25;
                var = log((base^varHigh - base^varLow) * varNorm + base^varLow)/log(base);
            else
                error('invalid scaling parameter')
            end
        end
        
        
        
        function res = calculateCenterLinearOris(cfgFileUtils)
            ori = cfgFileUtils.readParam('exp.cat.catCenter1', 'num');
            oris = [ori 1-ori];
            res = [CatStimulus.norm2var(oris(1), CatStimulus.ori_scale, CatStimulus.ori_low, CatStimulus.ori_high), ...
                   CatStimulus.norm2var(oris(2), CatStimulus.ori_scale, CatStimulus.ori_low, CatStimulus.ori_high)];
        end
        
        
        
        function res = calculateCenterLinearSFs(cfgFileUtils)
            sf = cfgFileUtils.readParam('exp.cat.catCenter2', 'num');
            sfs = [sf 1-sf];
            res = [CatStimulus.norm2var(sfs(1), CatStimulus.sf1_scale, CatStimulus.sf1_low, CatStimulus.sf1_high), ...
                   CatStimulus.norm2var(sfs(2), CatStimulus.sf1_scale, CatStimulus.sf1_low, CatStimulus.sf1_high)];
        end
        
    
    
        function names = getStimPropertyNames()
            names = ["ori_linear", "sf_linear", "sf_concentric", "gauss_notch_variability"];
        end
        
    end
    
end