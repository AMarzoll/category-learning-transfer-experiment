function showExperimentOverScreen(winPtr, dis, machine)
%Notify the subject that the experiment has ended.
    str = ['-- LAST EXPERIMENT SESSION FINISHED --\n\nThank you for your participation, this experiment has been completed.', ...
           'Please contact the experimenter.\n\nPress any key to exit.'];
           
    
    [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, str, dis.screenRect, 300, 100, true);
    Screen('TextSize', winPtr, newFontSize);
    DrawFormattedText(winPtr, str, 'center', 'center', dis.textColor);
    Screen('Flip', winPtr);
    Screen('TextSize', winPtr, oldFontSize);
    WaitSecs(1.5);
    KbStrokeWait(machine.kbIdx);
end