classdef OdStaircase < Staircase
    %generates n-down/m-up staircases for the orientation discrimination
    %task
    
    properties (SetAccess = private)
        initStepIdx
        initStepVal
        boundaryOri
        deviationDirections
        deltaOris
    end
    
    methods
        function obj = OdStaircase(exp, iOri)
            %for memory pre-allocation with empty parameters
            if isempty(exp)
                exp.od = [];
            end
           
            %call superclass constructor first for common data members
            obj = obj@Staircase(exp.od);
            
            if isempty(iOri)
                return;
            end
            
            obj.initStepIdx  = exp.od.initStepIdx;
            obj.initStepVal  = exp.od.initStepVal;
            obj.boundaryOri  = exp.od.boundaryOris(iOri);
            
            %calculate steps for the staircase procedure
            stepDist = [1:obj.nSteps] - obj.initStepIdx;
            obj.steps = obj.initStepVal * (1-obj.stepSizePerc).^stepDist;
            obj.curStepIdx = obj.initStepIdx;

            %randomize target orientations
            randIdx = randperm(obj.nTrialsPerBlock);
            deviationDirections = repmat([0 1], 1, ceil(obj.nTrialsPerBlock/2));
            if numel(deviationDirections) ~= obj.nTrialsPerBlock
                deviationDirections = deviationDirections(1:obj.nTrialsPerBlock);
            end
            deviationDirections = deviationDirections(randIdx);
            obj.deviationDirections = deviationDirections;
        end
        
        function [obj, ori] = nextTrial(obj, isLastCorrect)
            
            %call generic superclass method first
            obj = nextTrial@Staircase(obj, isLastCorrect);
            
            %calculate the deviation from the reference orientation
            if obj.deviationDirections(obj.curTrial) == 0
                obj.deltaOris(obj.curTrial) =  +obj.steps(obj.curStepIdx);
            else
                obj.deltaOris(obj.curTrial) =  -obj.steps(obj.curStepIdx);
            end
            ori = obj.boundaryOri + obj.deltaOris(obj.curTrial);
        end
        
        function res = getCurDeviationDirection(obj)
            res = obj.deviationDirections(obj.curTrial);
        end
        
        function res = getCurDeviationAngle(obj)
            res = obj.deltaOris(obj.curTrial);
        end
        
        function res = getAllDeviationAngles(obj)
            res = obj.deltaOris;
        end
        
        function thr = computeThreshold(obj, nReversals)
            %compute the the threshold for this block, using the geometric mean 
            %of the last N reversals
            
            %heavy lifting done in superclass method
            thr = computeThreshold@Staircase(nReversals, abs(obj.deltaOris));
        end
        
    end
end

