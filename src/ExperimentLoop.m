classdef (Abstract) ExperimentLoop 
    %This abstract class provides common functionalities for running the
    %experiment. The actual sequence of events still has to be implemented
    %by a subclass inheriting from this one.
    
    properties (SetAccess = private)
        winPtr
        machine
        subj
        dis
        exp
        saveManager
        startBlock
        subFieldStr
        
        fixCrossSize_pix
        fixCrossSurround_pix
        
        forcedBreakDuration_sec = 60
    end
    
    methods
        function obj = ExperimentLoop(winPtr, machine, subj, dis, exp, saveManager, startBlock, subFieldStr)
            obj.winPtr = winPtr;
            obj.machine = machine;
            obj.subj = subj;
            obj.dis = dis;
            obj.exp = exp;
            obj.saveManager = saveManager;
            obj.startBlock = startBlock;
            obj.subFieldStr = subFieldStr;
            
            %fixation cross pre-calculations
            obj.fixCrossSize_pix = obj.dis.deg2pix(obj.exp.fixCrossSize_deg);
            obj.fixCrossSurround_pix = obj.dis.deg2pix(obj.exp.fixCrossSurround_deg);
            if mod(obj.fixCrossSize_pix, 2) == 1
                obj.fixCrossSize_pix = obj.fixCrossSize_pix + 1;
            end
        end
    end
    
    methods (Access = protected)
        
        function drawFixationCross(obj, isCue)
            fixLineCoordsX_pix = [floor(-obj.fixCrossSize_pix/2) floor(obj.fixCrossSize_pix/2) 0 0];
            fixLineCoordsY_pix = [0 0 ceil(-obj.fixCrossSize_pix/2) ceil(obj.fixCrossSize_pix/2)];
            fixLineCoordsAll = [fixLineCoordsX_pix; fixLineCoordsY_pix];
            
            if isCue
                curFixCrossColor = obj.exp.fixCrossColor_cue;
            else
                curFixCrossColor = obj.exp.fixCrossColor;
            end
            
            Screen('FillOval', obj.winPtr, GrayIndex(obj.winPtr), CenterRect([0 0 obj.fixCrossSurround_pix obj.fixCrossSurround_pix], obj.dis.screenRect));
            Screen('DrawLines', obj.winPtr, fixLineCoordsAll, obj.exp.fixCrossLineWidth_pix, curFixCrossColor, [obj.dis.ctrX_pix, obj.dis.ctrY_pix], 2);
        end
        
        
        
        function showTitleScreen(obj, titleStr)
            [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(obj.winPtr, titleStr, obj.dis.screenRect, 300, 100, true);
            Screen('TextSize', obj.winPtr, newFontSize);
            DrawFormattedText(obj.winPtr, titleStr, 'center', 'center', obj.dis.textColor, 80, false, false, 1.5);
            Screen('Flip', obj.winPtr);
            Screen('TextSize', obj.winPtr, oldFontSize);
            
            WaitSecs(1.0);
            KbStrokeWait(obj.machine.kbIdx);
        end
        
        
        
        function showInstructionScreen(obj, instructionStr)
            [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(obj.winPtr, instructionStr, obj.dis.screenRect, 300, 100, true);
            Screen('TextSize', obj.winPtr, newFontSize);
            DrawFormattedText(obj.winPtr, instructionStr, 'center', 'center', obj.dis.textColor, 80, false, false, 1.5);
            Screen('Flip', obj.winPtr);
            Screen('TextSize', obj.winPtr, oldFontSize);
            
            WaitSecs(1.0);
            KbStrokeWait(obj.machine.kbIdx);
            obj.showCountdown();
        end
        
        
        
        function showCountdown(obj)
            for iNum = [3:-1:1]
                DrawFormattedText(obj.winPtr, num2str(iNum), 'center', 'center', obj.dis.textColor);
                if iNum == 3
                    vbl = Screen('Flip', obj.winPtr);
                else
                    vbl = Screen('Flip', obj.winPtr, vbl+1.0);
                end
            end
            WaitSecs(1.0);
        end
        
        
        
        function prepareResponseCollection(obj, validKeys)
            keyList = zeros(1, 256); 
            keyList(validKeys) = 1.0;
            KbQueueCreate(obj.machine.kbIdx, keyList);
        end
        
        
        
        function [respKey, isCorrect, isMiss, respTime_sec] = collectResponse(obj, startTime_sec, correctRespLambda)
            %side effect warning: calls KbQueueStop() and KbQueueFlush()
            isCorrect = false;
            isMiss = true;
            respKey = NaN;
            keyTime_sec = NaN;
            while GetSecs() < startTime_sec + obj.exp.(obj.subFieldStr).responseDur_sec
                [isPressed, pressOnsetTimes] = KbQueueCheck(obj.machine.kbIdx);
                if isPressed  
                    %check for first pressed key among valid keys
                    pressOnsetTimes(pressOnsetTimes == 0) = Inf;
                    [~, keyIdx] = min(pressOnsetTimes);
                    
                    %check if response is correct
                    isCorrect = correctRespLambda(keyIdx);
                    
                    isMiss = false;
                    keyTime_sec = pressOnsetTimes(keyIdx);
                    respKey = keyIdx;
                    break;
                end
                WaitSecs(0.01);
            end
            
            %prevent further response collection and remove unprocessed key
            %events
            KbQueueStop(obj.machine.kbIdx);
            KbQueueFlush(obj.machine.kbIdx);
            
            if isnan(keyTime_sec)
                respTime_sec = NaN;
            else
                respTime_sec = keyTime_sec - startTime_sec;
            end
        end
        
        
        
        function showFeedback(obj, isCorrect, isMiss)
            if isCorrect
                fbChar = hex2dec(obj.exp.posFeedbackChar);
                fbColor = obj.exp.posFeedbackColor;
            else
                fbChar = hex2dec(obj.exp.negFeedbackChar);
                fbColor = obj.exp.negFeedbackColor;
            end
            Screen('TextSize', obj.winPtr, obj.exp.feedbackTextSize);
            DrawFormattedText(obj.winPtr, fbChar, 'center', 'center', fbColor);
            Screen('Flip', obj.winPtr);
            Screen('TextSize', obj.winPtr, obj.dis.textSize);
            if isMiss
                Beeper();
            end
            WaitSecs(obj.exp.(obj.subFieldStr).feedbackDur_sec);
        end
        
        
        
        function [exitFlag, instructionFlag] = showPauseScreen(obj, suspensionFlag, forceBreakFlag)
            exitFlag = false;
            instructionFlag = false;
            pauseStartTime = clock();
            if suspensionFlag
                pauseStr = ['-- SUSPENSION MODE --\n\nNo responses have been recorded in a while. If you wish to read the instructions again, '...
                            'please click the button in the bottom left. Please continue by clicking the button below when you are ready.'];
            else
                pauseStr =  '-- PAUSE --\n\nYou can now take a short break. When you are ready, click the button below to continue.';
            end
            instructionButton = Button([0.20, 0.80], 'Repeat Instructions', obj.dis.screenRect);
            contButton        = Button([0.50, 0.80], 'Continue', obj.dis.screenRect);
            exitButton        = Button([0.75, 0.80], 'Emergency Exit', obj.dis.screenRect);
            exitButton        = exitButton.setTextColor([1.0 0.0 0.0]);
            ShowCursor(1);
            pauseStrOrig = pauseStr;
            while true
                if forceBreakFlag
                    timeSinceStart = etime(clock(), pauseStartTime);
                    if timeSinceStart < obj.forcedBreakDuration_sec
                        pauseStr = [pauseStrOrig, sprintf('\nTime until the next block can be started: %2.0f seconds', obj.forcedBreakDuration_sec-timeSinceStart)];
                    else
                        pauseStr = pauseStrOrig;
                    end
                end
                DrawFormattedText(obj.winPtr, pauseStr, 'center', 'center', obj.dis.textColor, 80, false, false, 1.5);
                if ~forceBreakFlag || (forceBreakFlag && (timeSinceStart >= obj.forcedBreakDuration_sec)) 
                    isInstructionButtonPressed = instructionButton.drawButton(obj.winPtr);
                    isContButtonPressed = contButton.drawButton(obj.winPtr);
                else
                    isInstructionButtonPressed = false;
                    isContButtonPressed = false;
                end
                isExitButtonPressed = exitButton.drawButton(obj.winPtr);
                Screen('Flip', obj.winPtr);
                if isInstructionButtonPressed
                    instructionFlag = true;
                    break;
                end
                if isContButtonPressed
                    break;
                end
                if isExitButtonPressed
                    exitFlag = true;
                    break;
                end
                
            end
            HideCursor();
        end
        
    end
end