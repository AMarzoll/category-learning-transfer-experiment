classdef Button
    %Simple class for rectangular buttons that change color when the mouse
    %cursor hovers over them.

    properties (Constant)
        defaultButtonHeight_norm     = 0.05;
        defaultButtonWidth_norm      = 0.10;
        defaultButtonColor           = [0.65, 0.65, 0.65];
        defaultButtonColor_mouseover = Button.defaultButtonColor * 1.25;
        defaultOutlineColor          = Button.defaultButtonColor * 0.75;
        defaultTextColor             = [0.0 0.0 0.0];
        buttonOutline_pix            = 2;
        
        
        norm2pixRect = @(rect_norm, screenRect_pix) round([rect_norm(1) * screenRect_pix(3), ...
                                                           rect_norm(2) * screenRect_pix(4), ...
                                                           rect_norm(3) * screenRect_pix(3), ...
                                                           rect_norm(4) * screenRect_pix(4)]);
    end
    
    properties (SetAccess = private)
        buttonCenter_norm
        buttonRect_norm
        buttonRect_pix
        buttonStr
        buttonHeight_norm     = Button.defaultButtonHeight_norm;
        buttonWidth_norm      = Button.defaultButtonWidth_norm;
        buttonColor           = Button.defaultButtonColor;
        buttonColor_mouseover = Button.defaultButtonColor_mouseover;
        outlineColor          = Button.defaultOutlineColor;
        textColor             = Button.defaultTextColor;
        screenRect_pix
    end
    
    methods
        function obj = Button(buttonCenter_norm, buttonStr, screenRect_pix)
            obj.buttonCenter_norm = buttonCenter_norm;
            obj.screenRect_pix = screenRect_pix;
            obj = obj.updateButtonRect();
            obj.buttonStr = buttonStr;
        end

        function obj = setButtonHeight(obj, newButtonHeight_norm)
            obj.buttonHeight_norm = newButtonHeight_norm;
            obj = obj.updateButtonRect();
        end
        
        function obj = setButtonWidth(obj, newButtonWidth_norm)
            obj.buttonWidth_norm = newButtonWidth_norm;
            obj = obj.updateButtonRect();
        end
        
        function obj = setButtonColor(obj, newButtonColor)
           obj.buttonColor = newButtonColor;
           obj.buttonColor_mouseover = min(1.25 * newButtonColor, 1.0);
           obj.outlineColor = max(0.75 * newButtonColor, 0.0);
        end
        
        function obj = setTextColor(obj, newTextColor)
            obj.textColor = newTextColor;
        end
        
        function isButtonPressed = drawButton(obj, winPtr)
            [mouseX, mouseY, mouseButtons] = GetMouse();
            isMousePressed = mouseButtons(1);
            isMouseOverButton = IsInRect(mouseX, mouseY, obj.buttonRect_pix);
            if isMousePressed && isMouseOverButton
                isButtonPressed = true;
            else
                isButtonPressed = false;
            end
            [textBounds_pix, oldFontSize, newFontSize] = adaptTextBoundsToRect(...
                winPtr, obj.buttonStr, obj.buttonRect_pix, 20, 3, false);
            textRect_pix = CenterRect(textBounds_pix, obj.buttonRect_pix);
            
            if isMouseOverButton
                buttonColor_current = obj.buttonColor_mouseover;
            else
                buttonColor_current = obj.buttonColor;
            end
            Screen('FillRect', winPtr, buttonColor_current, obj.buttonRect_pix);
            Screen('FrameRect', winPtr, obj.outlineColor, obj.buttonRect_pix, obj.buttonOutline_pix);
            Screen('TextSize', winPtr, newFontSize);
            Screen('DrawText', winPtr, obj.buttonStr, textRect_pix(1), textRect_pix(2), obj.textColor);
            Screen('TextSize', winPtr, oldFontSize);
        end
    end
    
    methods (Access = private)
        function obj = updateButtonRect(obj)
            obj.buttonRect_norm = [obj.buttonCenter_norm(1) - 0.5*obj.buttonWidth_norm, ...
                                   obj.buttonCenter_norm(2) - 0.5*obj.buttonHeight_norm, ...
                                   obj.buttonCenter_norm(1) + 0.5*obj.buttonWidth_norm, ...
                                   obj.buttonCenter_norm(2) + 0.5*obj.buttonHeight_norm];
            obj.buttonRect_pix = obj.norm2pixRect(obj.buttonRect_norm, obj.screenRect_pix);
        end
    end
end