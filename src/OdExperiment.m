classdef OdExperiment < ExperimentLoop
    %Experiment loop for the orientation discrimination task
    
    properties (Constant)
        instructionStr = ['-- ORIENTATION DISCRIMINATION TASK --\nINSTRUCTIONS\n\nIn the following task, we are asking you to make '...
                          'judgments on the orientation of grating patterns of alternating bright and dark bars. The orientation of these ' ...
                          'bars will be rotated from that of a reference grating (shown on the right) either counter-clockwise or clockwise by a few ' ...
                          'degrees. After each stimulus image has been presented, please indicate whether you think if the deviation was ' ...
                          'counter-clockwise (left-arrow key on your keyboard) or clockwise (right-arrow key). Please try to make your ' ...
                          'response as accurately as possible within five seconds. If you are unsure, please try to guess as well as you can ' ...
                          'and try to avoid missed trials (indicated by a beeping sound). You will be informed after each trial if your ' ...
                          'response was correct or not. Please keep your eyes fixated on the center of the screen, indicated by a cross. ' ...
                          'A brief flash of this cross will indicate the next trial''s onset.\n\nWhen you are ready, press any key to ' ...
                          'continue.'];
        nFlipsPerTrial = 6;
    end
    
    properties (SetAccess = private)
        odStartOri
    end
    
    methods
        
        function obj = OdExperiment(winPtr, machine, subj, dis, exp, saveManager, startBlock, odStartOri)
            obj = obj@ExperimentLoop(winPtr, machine, subj, dis, exp, saveManager, startBlock, 'od');
            obj.odStartOri = odStartOri;
        end
        
        
        function [res, exitFlag] = run(obj, res)
            showInstructions = true;
            HideCursor();
            exitFlag = false;
            
            %KbQueue preparation
            validKeys = [obj.exp.od.ccwKey, obj.exp.od.cwKey];
            obj.prepareResponseCollection(validKeys);
            
            %determine order of orientations
            if obj.exp.od.firstOri == 1
                oris = [1 2];
            else
                oris = [2 1];
            end
            oriCounter = 1;
            
            while oris(1) ~= obj.odStartOri
                oris(1) = [];
                oriCounter = oriCounter + 1;
            end
            
            if isempty(oris)
                error('Empty orientation array')
            end
            
            
            %Experiment loop
            for iOri = oris
                titleStr = sprintf('-- ORIENTATION DISCRIMINATION TASK - PART %u --', oriCounter);
                obj.showTitleScreen(titleStr);
                
                for iBlock = obj.startBlock:obj.exp.od.nBlocks
                    
                    %new staircase
                    staircase = OdStaircase(obj.exp, iOri);
                    isLastCorrect = true; %default value for first trial
                    
                    %show countdown
                    if iBlock ~= obj.startBlock
                        obj.showCountdown();
                    end
                    
                    for iTrial = 1:obj.exp.od.nTrialsPerBlock
                        iFlip = 1; %for timestamp indexing
                        
                        %display instructions
                        if showInstructions
                            obj.showInstructionScreen(OdExperiment.instructionStr, iOri);
                            showInstructions = false;
                        end
                        
                        %compute trial parameters
                        [staircase, nextOri] = staircase.nextTrial(isLastCorrect);
                        
                        %generate stimulus and noise images
                        stimulus = OdStimulus(nextOri, obj.exp.od.sfs(iOri), obj.dis);
                        stimulusImage = stimulus.generateImage();
                        noiseImage = stimulus.generateNoise();
                        res.od.stimuli(iOri, iBlock, iTrial) = stimulus;
                        
                        %save stimulus properties in result struct
                        res.od.trialTable(iOri, iBlock, iTrial, 1) = iTrial;
                        res.od.trialTable(iOri, iBlock, iTrial, 2) = obj.exp.od.boundaryOris(iOri);
                        res.od.trialTable(iOri, iBlock, iTrial, 3) = staircase.getCurDeviationDirection();
                        res.od.trialTable(iOri, iBlock, iTrial, 4) = staircase.getCurDeviationAngle();
                        
                        %get vbl
                        vbl = Screen('Flip', obj.winPtr);
                        
                        %preCue
                        obj.drawFixationCross(false);
                        flipWrapper(obj.dis.frameDur_sec);
                        
                        %cue
                        obj.drawFixationCross(true);
                        flipWrapper(obj.exp.od.preCueDur_sec);
                        
                        %postCue
                        obj.drawFixationCross(false);
                        flipWrapper(obj.exp.od.cueDur_sec);
                        
                        %stimulus
                        texPtr = Screen('MakeTexture', obj.winPtr, stimulusImage, [], [], obj.dis.floatPrecision);
                        Screen('DrawTexture', obj.winPtr, texPtr);
                        obj.drawFixationCross(false);
                        flipWrapper(obj.exp.od.postCueDur_sec);
                        Screen('Close', texPtr);
                        
                        %record button presses from stimulus onset
                        KbQueueStart(obj.machine.kbIdx);
                        respStartTime_sec = vbl;
                        
                        %backward masking
                        texPtr = Screen('MakeTexture', obj.winPtr, noiseImage, [], [], obj.dis.floatPrecision);
                        Screen('DrawTexture', obj.winPtr, texPtr);
                        flipWrapper(obj.exp.od.stimDur_sec);
                        Screen('Close', texPtr);
                        
                        %response period
                        obj.drawFixationCross(false);
                        flipWrapper(obj.exp.od.backwardMaskDur_sec);
                        
                        %record response
                        correctRespLambda = @(key)obj.exp.od.key2ori(key) == staircase.getCurDeviationDirection();
                        [respKey, isCorrect, isMiss, respTime_sec] = obj.collectResponse(respStartTime_sec, correctRespLambda);
                        
                        %make entries in result struct
                        if ~isMiss
                            res.od.trialTable(iOri, iBlock, iTrial, 5) = respKey == obj.exp.od.cwKey; %0 for counter-clockwise, 1 for clockwise
                        else
                            res.od.trialTable(iOri, iBlock, iTrial, 5) = NaN; %NaN for a missed trial
                        end
                        res.od.trialTable(iOri, iBlock, iTrial, 6) = isCorrect;
                        res.od.trialTable(iOri, iBlock, iTrial, 7) = isMiss;
                        res.od.trialTable(iOri, iBlock, iTrial, 8) = respTime_sec;
                        
                        %show feedback
                        obj.showFeedback(isCorrect, isMiss);
                        
                        %suspend experiment if there are three misses in a row
                        if (iTrial >= 3) && all(res.od.trialTable(iOri, iBlock, iTrial-2:iTrial, 7) == true) && (iTrial ~= obj.exp.od.nTrialsPerBlock)
                            [exitFlag, instructionFlag] = obj.showPauseScreen(true, false);
                            if instructionFlag
                                showInstructions = true;
                            end
                            if exitFlag
                                return;
                            end
                        end
                        
                        isLastCorrect = isCorrect;
                    end
                    
                    %save staircase object - just in case
                    res.od.staircases(iOri, iBlock) = staircase;
                    
                    %do block summary statistics
                    res.od.blockTable(iOri, iBlock, 1) = iBlock;
                    res.od.blockTable(iOri, iBlock, 2) = obj.exp.od.boundaryOris(iOri);
                    res.od.blockTable(iOri, iBlock, 3) = geomean(abs(staircase.getAllDeviationAngles()));
                    res.od.blockTable(iOri, iBlock, 4) = staircase.computeThreshold(res.od.nReversals);
                    res.od.blockTable(iOri, iBlock, 5) = sum(res.od.trialTable(iOri, iBlock, :, 7), 'omitnan');
                    res.od.blockTable(iOri, iBlock, 6) = mean(res.od.trialTable(iOri, iBlock, :, 8), 'omitnan');
                    res.od.blockTable(iOri, iBlock, 7) = log10(mean(res.od.trialTable(iOri, iBlock, :, 5), 'omitnan') / 0.5);
                    
                    %do autosave
                    obj.saveManager.createAutosaveFile(obj.machine, obj.subj, obj.dis, obj.exp, res);
                    
                    %show pause screen (except on last block)
                    if iBlock ~= obj.exp.od.nBlocks
                        [exitFlag, instructionFlag] = obj.showPauseScreen(false, true);
                        if exitFlag
                            return;
                        end
                        if instructionFlag
                            showInstructions = true;
                        end
                    end
                    
                    Screen('FillRect', obj.winPtr, [0.5 0.5 0.5], [0 0 obj.dis.screenRect(3) obj.dis.screenRect(4)]);
                    Screen('Flip', obj.winPtr);
                end
                showInstructions = true;
                oriCounter = oriCounter + 1;
            end
            KbQueueRelease();
            
            %subroutine for reducing visual clutter
            function flipWrapper(time)
                [vbl, res.od.sot(iOri, iBlock, iTrial, iFlip), res.od.ft(iOri, iBlock, iTrial, iFlip), res.od.ms(iOri, iBlock, iTrial, iFlip)] = ...
                    Screen('Flip', obj.winPtr, vbl+time-obj.dis.hfd);
                iFlip = iFlip + 1;
            end
        end
        
    end
    
    methods (Access = protected)
        
        function showInstructionScreen(obj, instructionStr, iOri)
            %draw instruction text on the left part of the screen
            norm2pixRect = @(rect_norm)round([rect_norm(1) * obj.dis.screenRect(3), ...
                                              rect_norm(2) * obj.dis.screenRect(4), ...
                                              rect_norm(3) * obj.dis.screenRect(3), ...
                                              rect_norm(4) * obj.dis.screenRect(4)]);
            instructionTextRect_norm = [0, 0, 0.6, 1];
            instructionTextRect_pix = norm2pixRect(instructionTextRect_norm);
            [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(obj.winPtr, instructionStr, instructionTextRect_pix, 100, 100, true);
            Screen('TextSize', obj.winPtr, newFontSize);
            DrawFormattedText(obj.winPtr, instructionStr, 'center', 'center', obj.dis.textColor, 80, false, false, 1.5, 0, instructionTextRect_pix);
            Screen('TextSize', obj.winPtr, oldFontSize);
            
            %draw reference stimulus on the right
            sampleStimulus = OdStimulus(obj.exp.od.boundaryOris(iOri), obj.exp.od.sfs(iOri), obj.dis);
            sampleStimulus = sampleStimulus.setGaborContrast(0.7);
            stimulusImage = sampleStimulus.generateImage();
            sampleStimulusCanvasRect_norm = [0.6, 0, 1, 1];
            sampleStimulusCanvasRect_pix = norm2pixRect(sampleStimulusCanvasRect_norm);
            sampleStimulusRect_pix = CenterRect([0 0 size(stimulusImage, 1) size(stimulusImage, 2)], sampleStimulusCanvasRect_pix);
            texPtr = Screen('MakeTexture', obj.winPtr, stimulusImage, [], [], obj.dis.floatPrecision);
            Screen('DrawTexture', obj.winPtr, texPtr, [], sampleStimulusRect_pix);
            
            %show on screen and wait for response
            Screen('Flip', obj.winPtr);
            Screen('Close', texPtr);
            WaitSecs(1.0);
            KbStrokeWait(obj.machine.kbIdx);
            obj.showCountdown();
            KbQueueFlush(obj.machine.kbIdx);
        end
        
    end
end

