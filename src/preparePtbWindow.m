function dis = preparePtbWindow()
%Do some PTB stuff and save some variables for later use in the 'dis'
%struct

    Screen('Preference', 'SkipSyncTests', 2);
    if exist('onCleanup','class')
        oC_Obj = onCleanup(@()sca);
    end
    PsychImaging('PrepareConfiguration');
    PsychImaging('AddTask', 'General', 'FloatingPoint32BitIfPossible'); %set-up a 32bit floating point framebuffer
    dis.applyAlsoToMakeTexture = 1;
    PsychImaging('AddTask', 'General', 'NormalizedHighresColorRange', dis.applyAlsoToMakeTexture); %normalize the color range ([0 1] corresponds to [min max])
    PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma');
    if ismac
        PsychImaging('AddTask', 'General', 'UseRetinaResolution');
    end
    dis.whichScreen = max(Screen('screens'));
    dis.floatPrecision = 2;
    dis.assumedBlindspot_deg = 13.5; %after Li et al., 2020 (Scientific Reports, https://doi.org/10.1038/s41598-019-57204-1)
end