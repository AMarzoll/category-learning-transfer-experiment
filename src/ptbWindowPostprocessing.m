function dis = ptbWindowPostprocessing(winPtr, dis)
%Do some more stuff after the PTB windows has been opened and save relevant
%variables in the 'dis' struct.

dis.gamma = 2.2; %assumed average gamma for consumer grade displays
PsychColorCorrection('SetEncodingGamma', winPtr, 1/dis.gamma);
Screen('BlendFunction', winPtr, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
dis.frameDur_sec = 1/FrameRate(winPtr); %get reciprocal framerate
dis.hfd = dis.frameDur_sec/2; %halve frame duration for better stimulus timing
dis.windowInfo = Screen('GetWindowInfo', winPtr);
dis.textSize = 26;
dis.textColor = [0.0, 0.0, 0.0];
Screen('TextSize', winPtr, dis.textSize); %font size
Priority(MaxPriority(dis.whichScreen));
[dis.ctrX_pix, dis.ctrY_pix] = RectCenter(dis.screenRect);
end
