function res = osxCharRemap(charNum)
%MacOSX keys are now mapped to Windows keys for some reason. This function
%takes a number representing a char and converts it from the OSX mapping to
%the Windows mapping
    if ismac
        keysWin = KbName('KeyNamesWindows');
        keysOsx = KbName('KeyNamesOSX');
        
        keyToFind = keysOsx{charNum};
        findFun = @(x)strcmp(x, keyToFind);
        
        res = find(cellfun(findFun, keysWin));
    else
        res = charNum;
    end
end

