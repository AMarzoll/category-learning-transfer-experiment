function machine = retrieveMachineParameters()
%Retrieves some basic parameters of this computer.

machine.OS = computer;
if ~ispc && ~ismac
    error('Unsupported operating system');
end

machine.expSubDir = 'visual_experiment';
machine.randomSeed = rng('shuffle');
end

