function hasMailFailed = sendResultsByMail(msgStruct, senderStruct, res, saveFilePath)
%Sends the result file by mail and also puts a little summary in the mail
%text body.

    saveFilePath_zipped = strcat(saveFilePath(1:end-3), 'zip');
    hasMailFailed = false;
    
    catResultSummary = genResultSummary('cat', '--CATEGORIZATION TASK RESULTS--');
    odResultSummary  = genResultSummary('od', '--OD TASK RESULTS--');

    mailErrorFun = @(x) true;
    zip(saveFilePath_zipped, saveFilePath); %mail service does not like naked .mat files
    
    msgStruct.subject = 'Experiment Results';
    msgStruct.textbody = [catResultSummary, newline(), odResultSummary];
    msgStruct.attachments = saveFilePath_zipped;
    
    failureFlag = sendMailAlert(msgStruct, senderStruct, mailErrorFun);

    if ~isempty(failureFlag)
        if failureFlag
            hasMailFailed = true;
        end
    else
        hasMailFailed = false;
    end
    
    %subroutine for formatting results into char array
    function resultSummary = genResultSummary(fieldName, title)
        if isfield(res, fieldName)
            
            %turn three-dimensional matrix two-dimensional to display it
            if ndims(res.(fieldName).blockTable) == 3 
                res.(fieldName).blockTable = squeeze(reshape(permute(res.(fieldName).blockTable, [2 1 3]), 1, [], size(res.(fieldName).blockTable, 3)));
            end
            
            %elaborate string re-formatting
            resultSummary = [res.(fieldName).blockTableLabels; string(res.(fieldName).blockTable)];
            missIdx = ismissing(resultSummary);
            resultSummary(missIdx) = "N/A";
            resultSummary = compose(join(resultSummary, '\t'));
            resultSummary = reshape(resultSummary, 1, []);
            resultSummary = convertStringsToChars(resultSummary);
            resultSummary = [title, newline(), resultSummary];
        else
            resultSummary = '';
        end
    end
end