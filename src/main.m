function endStr = main(context)
%This is the entry-point function for the Matlab app and the most
%high-level function for the experiment.
try
    %debug mode flag (skip some steps and don't send error e-mails)
    debugMode = false;
    
    %message for app window
    endStr = 'Experiment appears to have terminated prematurely. Please restart session or contact experimenter';
    
    %figure out some machine properties
    machine = retrieveMachineParameters();
    if ispc
        userDir = getenv('USERPROFILE');
    else
        userDir = getenv('HOME');
    end
    
    %choose text renderer
    Screen('Preference', 'TextRenderer', context.textRenderer);
    
    %set up helper for config file IO
    cfgFileUtils = CfgFileUtils(machine);
    
    %get e-mail stuff for reporting results and errors
    msgStruct.address = cfgFileUtils.readParam('experimenterMail', 'char');
    
    senderStruct.address = cfgFileUtils.readParam('botMail', 'char'); %sender mail account, use throwaway
    senderStruct.password = cfgFileUtils.readParam('botPhrase', 'char'); %password to mail account
    senderStruct.hostSmtp = cfgFileUtils.readParam('botHostSmtp', 'char'); %SMTP host
    senderStruct.portSmtp  = cfgFileUtils.readParam('botHostPort', 'char'); %SMTP port
    
    %subject- and session-specific parameters
    subj.startTime = now();
    subj.startTimeStr = datestr(subj.startTime);
    subj.ID = cfgFileUtils.readParam('subj.ID', 'char');
    if isnan(context.sessionNumOverride)
        if ~cfgFileUtils.doesParamExist('subj.nSessionsCompleted')
            cfgFileUtils.writeParam('subj.nSessionsCompleted', 0);
        end
    else
        cfgFileUtils.writeParam('subj.nSessionsCompleted', context.sessionNumOverride - 1);
    end
    subj.nSessionsCompleted = cfgFileUtils.readParam('subj.nSessionsCompleted', 'num');
    
    %supress key presses bleeding through to the underlying program (app, matlab IDE)
    ListenChar(-1);
    
    %set up save file manager
    saveManager = SaveManager(machine, subj, cfgFileUtils);
    
    %set up display
    dis = preparePtbWindow();
    [winPtr, dis.screenRect] = PsychImaging('OpenWindow', dis.whichScreen, 0.5, [], 32, 2, [], [], kPsychNeed32BPCFloat);
    dis = ptbWindowPostprocessing(winPtr, dis);
    
    %autosave restore functionality
    catStartBlock = 1; %start experiment with the first block - default if no autosave is loaded
    odStartBlock = 1;
    odStartOri = cfgFileUtils.readParam('exp.od.firstOri', 'num');
    [isAutosaveFilePresent, saveManager] = saveManager.checkAutosaveFilePresence();
    restoreFlag = false;
    if(isAutosaveFilePresent)
        %if there is at least one autosave file, let subject select whether
        %to reload the session
        [restoreFlag, restoreFileName, exitFlag] = saveManager.showAutosaveRestoreScreen(winPtr);
        if exitFlag
            endStr = 'Experiment aborted by user.';
            sca();
            ListenChar(0);
            return;
        end
        if restoreFlag
            [machine, subj, dis, exp, res, catStartBlock, odStartBlock, odStartOri, catTaskFlag, odTaskFlag] = ...
                saveManager.loadAutosaveFile(restoreFileName);
            saveManager = SaveManager(machine, subj, cfgFileUtils); %recreate save manager with restored session variables
        end
    end
    
    %determine keyboard device ID (in case PTB auto-detects the wrong one for the Kb... functions)
    machine.kbIdx = keyboardTest(winPtr, dis);
    
    %show welcome screen
    exitFlag = showReminderScreen(winPtr, dis);
    if exitFlag
        endStr = 'Experiment aborted by user.';
        sca();
        ListenChar(0);
        return;
    end
    
    %find out whether screen distance needs to be measured, but only once
    if cfgFileUtils.doesParamExist('dis.pixPerCm')
        %if screen size has already been measured, load from file
        dis.pixPerCm = cfgFileUtils.readParam('dis.pixPerCm', 'num');
    else
        %measure screen size and subject distance
        dis.pixPerCm = pixPerCmEstimation(winPtr, machine.kbIdx);
        cfgFileUtils.writeParam('dis.pixPerCm', dis.pixPerCm);
    end
    
    %measure subject blind spot to approximate screen distance (each time)
    if ~debugMode
        dis.blindspotOffsets_pix = blindSpotDistanceEstimation(winPtr, machine.kbIdx, context.dotShader);
        dis.screenDist_cm = mean(dis.blindspotOffsets_pix) / (dis.pixPerCm * tand(dis.assumedBlindspot_deg));
    else
        dis.screenDist_cm = 80;
    end
    dis.deg2pix = @(deg) (2*dis.screenDist_cm*tand(0.5*deg)).*dis.pixPerCm; %convenience lambda for later use
    
    %set up parameter and result structs, if necessary
    if ~restoreFlag
        %define experiment
        [exp, catTaskFlag, odTaskFlag] = defineExperimentParameters(dis, subj, cfgFileUtils);
        
        %pre-allocate result struct
        res = defineResultStruct(exp, catTaskFlag, odTaskFlag);
    end
    
    %run orientation discrimination task
    if odTaskFlag && ~isempty(odStartBlock)
       odExperiment = OdExperiment(winPtr, machine, subj, dis, exp, saveManager, odStartBlock, odStartOri);
       [res, exitFlag] = odExperiment.run(res);
       if exitFlag
           sca();
           ListenChar(0);
           return;
       end
    end
    
    %run main experiment
    if catTaskFlag && ~isempty(catStartBlock)
        catExperiment = CatExperiment(winPtr, machine, subj, dis, exp, saveManager, catStartBlock);
        [res, exitFlag] = catExperiment.run(res);
        if exitFlag
            sca();
            ListenChar(0);
            return;
        end
    end
    
    %log end of session
    subj.finishTime = now();
    
    %save results and important information
    saveFilePath = saveManager.createSaveFile(machine, subj, dis, exp, res);
    
    %safely delete autosave
    saveManager.deleteAutosaveFile();
    
    %increase numbers of completed sessions
    cfgFileUtils.writeParam('subj.nSessionsCompleted', subj.nSessionsCompleted + 1);
    
    %send e-mail alert with zipped data file
    hasMailFailed = sendResultsByMail(msgStruct, senderStruct, res, saveFilePath);
    
    %inform subject that the session is properly over
    showFinalScreen(winPtr, dis, machine, hasMailFailed);
    
    %inform subject if experiment has been completed
    if (subj.nSessionsCompleted + 1) == exp.lastSession
        showExperimentOverScreen(winPtr, dis, machine)
    end
    
    %message for app window
    endStr = 'Experiment completed successfully. Thank you for your continued participation. You may now close this window.';
    
    %re-enable key-presses in matlab
    ListenChar(0);
    
    sca();
    
catch xcp
    %store exception message in log-file (in case e-mail doesn't work)
    errMsg = getReport(xcp, 'extended', 'hyperlinks', 'off');
    logFilePath = fullfile(userDir, machine.expSubDir, 'errorlog.txt');
    fId = fopen(logFilePath, 'w');
    fwrite(fId, errMsg);
    fclose(fId);
    %send mail with error message if possible
    if(exist('msgStruct', 'var') && ...
            isfield(msgStruct, 'address') && ...
            exist('senderStruct', 'var') && ...
            all(isfield(senderStruct, {'address', 'password', 'hostSmtp', 'portSmtp'})) && ...
            ~debugMode)
        msgStruct.subject = 'Experiment Error!';
        msgStruct.textbody = errMsg;
        sendMailAlert(msgStruct, senderStruct);
    else
        fprintf(errMsg);
    end
    sca();
    ListenChar(0);
    rethrow(xcp)
end
end
