classdef CatStaircase < Staircase
    %generates n-down/m-up staircases for the category learning task

    properties (SetAccess = private)
        catCenter
        catRadius
        catBoundaryNormVec
        initStep
        categories %contains the category for each trial
        boundaryDistances %will contain the actual subject performance
    end
    
    methods
        function obj = CatStaircase(exp)
            %call superclass constructor first for common data members
            obj = obj@Staircase(exp.cat);

            obj.initStep           = exp.cat.initStep;
            obj.catCenter          = exp.cat.catCenter;
            obj.catRadius          = exp.cat.catRadius;
            obj.catBoundaryNormVec = exp.cat.catBoundaryNormVec;
            
            %calculate steps for the staircase procedure
            obj.steps(1) = exp.cat.catRadius - 0.01*exp.cat.catRadius; %small offset to avoid special case with random parameter generation
            obj.steps(2:obj.nSteps) = obj.steps(1) * (1-obj.stepSizePerc).^[1:obj.nSteps-1];
            obj.curStepIdx = obj.initStep;
            obj.boundaryDistances = nan(1, obj.nTrialsPerBlock);
            
            %randomize target categories
            randIdx = randperm(obj.nTrialsPerBlock);
            categories = repmat([0 1], 1, ceil(obj.nTrialsPerBlock/2));
            if numel(categories) ~= obj.nTrialsPerBlock
                categories = categories(1:obj.nTrialsPerBlock);
            end
            categories = categories(randIdx);
            obj.categories = categories;
        end
        
        
        
        function [obj, stimParams] = nextTrial(obj, isLastCorrect)
            %generate stimulus parameters for the next trial given
            %performance history in the staircase procedure
            
            %call generic superclass method first
            obj = nextTrial@Staircase(obj, isLastCorrect);
            
            %save the actual category boundary distance for this trial and
            %get corresponding normalized stimulus parameters
            obj.boundaryDistances(obj.curTrial) = obj.steps(obj.curStepIdx);
            stimParams = obj.generateStimParams(obj.boundaryDistances(obj.curTrial));
        end
        
        
        
        function cat = getCurCat(obj)
           cat = obj.categories(obj.curTrial);
        end
        
        
        
        function boundDist = getCurBoundaryDist(obj)
           boundDist = obj.boundaryDistances(obj.curTrial);
        end
        
        
        
        function boundDists = getAllBoundaryDistances(obj)
            boundDists = obj.boundaryDistances;
        end
        
        
        
        function [thr, nTotalReversals] = computeThreshold(obj, nReversals)
            %compute the the threshold for this block, using the geometric mean 
            %of the last N reversals
            
            %heavy lifting done in superclass method
            [thr, nTotalReversals] = computeThreshold@Staircase(nReversals, obj.boundaryDistances);
        end
          
    end
    
    methods (Access = private)
        function stimParams = generateStimParams(obj, boundaryDist)
            %generate 4 random normalized stimulus parameters in accordance with staircase constraints
            
            %define direction from boundary
            if obj.categories(obj.curTrial) == 0
                targetDeltaD = -boundaryDist;
            elseif obj.categories(obj.curTrial) == 1
                targetDeltaD = boundaryDist;
            else
                error('Invalid code for target category: must be 0 or 1')
            end
            
            boundaryConstant = dot(obj.catCenter, obj.catBoundaryNormVec);
            isInHypersphere = @(pt) ((pt(1)-obj.catCenter(1))^2 + ...
                                     (pt(2)-obj.catCenter(2))^2 + ...
                                     (pt(3)-obj.catCenter(3))^2 + ...
                                     (pt(4)-obj.catCenter(4))^2) ...
                                     <= obj.catRadius^2;

            %generate candidate points until a valid result has been found
            while true
                %generate 3 parameters randomly, calculate the remaining one
                x = (obj.catCenter(1)-obj.catRadius)+(2*obj.catRadius*rand);
                y = (obj.catCenter(2)-obj.catRadius)+(2*obj.catRadius*rand);
                z = (obj.catCenter(3)-obj.catRadius)+(2*obj.catRadius*rand);
                w = -(dot(obj.catBoundaryNormVec(1:3), [x y z]) - (boundaryConstant + targetDeltaD))/obj.catBoundaryNormVec(4);
                candidate = [x y z w];
                if isInHypersphere(candidate)
                   stimParams = candidate;
                   break;
                end
            end
        end
    end
    
    
    methods (Static)
        function thresholds = simulateRandomPerformance(B, nReversals, exp)
            thresholds = nan(1,B);
            for iSim = 1:B
                sc = CatStaircase(exp);
                randResps = rand(1, exp.cat.nTrialsPerBlock) > 0.5;
                for iTrial = 1:exp.cat.nTrialsPerBlock
                    sc = sc.nextTrial(randResps(iTrial));
                end
                thresholds(iSim) = sc.computeThreshold(nReversals);
            end
            
        end
    end
end