function msgStr = fixDependencies()
%to replace PsychStartup(), which does not work with standalone desktop
%apps. Currently only does windows stuff

msgStr = [];

if ispc
    
    path = getenv('PATH');
    
    %% locate path to gstreamer
    if Is64Bit()
        gstreamerDir = getenv('GSTREAMER_1_0_ROOT_X86_64');
        gstreamerSubDirs = ["x86_64", "msvc_x86_64"];
    else
        gstreamerDir = getenv('GSTREAMER_1_0_ROOT_X86');
        gstreamerSubDirs = ["x86", "msvc_x86"];
    end
    
    %if path cannot be found via environment variable, look in likely locations
    if isempty(gstreamerDir)
        for driveLetter = 'A':'Z'
            if exist([driveLetter, ':\gstreamer\1.0\'], 'dir')
                for iSD = 1:numel(gstreamerSubDirs)
                    testDir = [driveLetter ':\gstreamer\1.0\' convertStringsToChars(gstreamerSubDirs(iSD))];
                    if exist(testDir, 'dir')
                        gstreamerDir = testDir;
                        break;
                    end
                end
            end
        end
    end
    
    if isempty(gstreamerDir) || ~exist(gstreamerDir, 'dir')
        error(['Could not locate gstreamer. Please install gstreamer in default location or try restarting computer. ' ...
               'If problem persists, please contact experimenter']);
    end
    
    gstreamerBinPath = fullfile(gstreamerDir, 'bin');
    
    if ~exist(fullfile(gstreamerBinPath, 'gstreamer-1.0.0.dll'), 'file') && exist(fullfile(gstreamerBinPath, 'libgstreamer-1.0.0.dll'), 'file')
        error(['Wrong version of gstreamer installed (MinGW version)! Please install the MSVC version instead. ' ...
               'Contact experimenter if assistance is needed.'])
    end
    
    if ~contains(path, gstreamerBinPath, 'IgnoreCase', true)
        pathPrepended = [gstreamerBinPath, ';', path];
        setenv('PATH', pathPrepended);
    end
   
    %% check for matlab runtime in PATH
    path = getenv('PATH');
    pattern = '[A-Z]:[\\/]Program Files[\\/]MATLAB[\\/]MATLAB Runtime[\\/]v[0-9]+[\\/]bin'; %example path: C:\Program Files\MATLAB\MATLAB Runtime\v98\bin
    isRuntimeInPath = ~isempty(regexp(path, pattern, 'ONCE'));
    
    %if runtime is not in path, look for it manually in default location
    if ~isRuntimeInPath
        runtimeBinPath = [];
        for driveLetter = 'A':'Z'
            if exist([driveLetter, ':\Program Files\MATLAB\MATLAB Runtime'], 'dir')
                %matlab runtime was found, create entry in PATH
                folders = dir([driveLetter ':\Program Files\MATLAB\MATLAB Runtime\v*']);
                if isempty(folders)
                    continue;
                end
                runtimeBinPath = fullfile(folders(end).folder, folders(end).name, 'bin');
                pathPrepended = [runtimeBinPath, ';', path];
                setenv('PATH', pathPrepended);
                break;
            end
        end
        
        if isempty(runtimeBinPath)
            error(['Could not locate MATLAB Runtime. Please install the 64-bit MATLAB Runtime in default location, ' ...
                   'e.g. C:\Program Files\MATLAB\MATLAB Runtime\']);
       end
    end
    msgStr = 'Fixes successfully applied where necessary.';
else
    msgStr = 'No fixes performed - not necessary on this operating system.';
end



end