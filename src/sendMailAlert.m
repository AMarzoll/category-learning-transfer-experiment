function errCallbackOutput = sendMailAlert(varargin)

% attempts to send an e-mail alert when called and (hopefully) doesn't crash
% your program when it fails to do so. Please don't overuse so that the mail
% account doesn't get banned for spam activity. Convenience wrapper around
% sendmail().
%
% Usage:
% varargout = sendMailAlert(msgStruct, senderStruct [, errorFunHandle] [, errorFunParams])
%
% Example:
% [winPtr, ~] = PsychImaging('OpenWindow',...
% ... stuff happens here ...
% sendMailAlert('myaddress@brown.edu','Almost done!','100 trials until subject is finished' [], @exampleErrorFun, winPtr);
%
% Input arguments:
% msgStruct: must be a struct with the following fields:
%   address: must be a char vector with the recipient's email address
%   subject (optional): must be a char vector the the mail subject line
%   textbody (optional): must be a char vector/string array/cell containing strings of the mail text body
%   attachments (optional): must be a char vector or a cell array of char
%       vectors containing valid file paths to files to be sent as attachments
%
% senderStruct: must be a struct with the following fields:
%   address: must be a char vector with the senders's email address
%   password: must be a char vector with the sender's email password
%   hostSmtp: must be a char vector with the url for the sender's e-mail
%       provider smtp host
%   portSmtp: must be char vector (!) with the port for the senders's e-mail
%       provider smtp host
%
% errorFunHandle (optional): handle to a function that gets called when
% email notification fails
%
% errorFunParams (optional): parameters to be passed to the error function
% specified by errorFunHandle
%
% Output arguments:
% errCallbackOutput: Output argument specified by the (optionally) supplied error callback function
%
% AM 2018/06/30: Made this thing
% AM 2020/10/19: Enabled output for error callback, allow strings in
%                message body, changed e-mail provider to gmail
% AM 2020/10/20: Modified to supply sender mail account information from
%                the outside, information now to be supplied as structs

try
    %initialize callback stuff
    errCallbackOutput = [];
    errorFunHandle = [];
    errorFunParams = [];
    
    %handle input, throw errors for some common error types
    
    %handle custom error function first
    if nargin >= 3
        errorFunHandle = varargin{3};
        if ~isa(errorFunHandle, 'function_handle')
            errorFunHandle = [];
            error('invalid handle to error function')
        end
        if nargin >= 4
            errorFunParams = varargin{4};
        end
    end
    
    %handle other input arguments
    if nargin >= 2 && nargin < 5
        msgStruct = varargin{1};
        senderStruct = varargin{2};
        
        %basic checks for the message struct contents
        if ~isstruct(msgStruct)
            error('message information must be contained in a struct!')
        end
        if ~ischar(msgStruct.address)
            error('recipient email address is not a character vector')
        end
        if isempty(msgStruct.address)
            error('recipient email address is empty')
        end
        atPos = strfind(msgStruct.address,'@'); %just check the most basic stuff to avoid regexp nightmare
        if numel(atPos) ~= 1
            error('invalid recipient email address character vector?');
        end
        dotPos = strfind(msgStruct.address,'.');
        if ~any(dotPos > atPos)
            error('invalid recipient email address character vector?');
        end
        if isfield(msgStruct, 'subject') && ~isempty(msgStruct.subject)
            if ~ischar(msgStruct.subject) && ~isempty(msgStruct.subject)
                warning('mail subject is not a character vector')
                msgStruct.subject = 'Alert from MATLAB';
            end
            if isempty(msgStruct.subject)
                msgStruct.subject = 'Alert from MATLAB';
            end
        else
            msgStruct.subject = 'Alert from MATLAB';
        end
        if isfield(msgStruct, 'textbody') && ~isempty(msgStruct.textbody)
            if ~(ischar(msgStruct.textbody) || isstring(msgStruct.textbody) || iscell(msgStruct.textbody)) && ~isempty(msgStruct.textbody)
                warning('mail text body is not a character vector or string array')
                msgStruct.textbody = [];
            end
        else
            msgStruct.textbody = [];
        end
        if isfield(msgStruct, 'attachments') && ~isempty(msgStruct.attachments)
            if (~ischar(msgStruct.attachments) && ~iscell(msgStruct.attachments)) && ~isempty(msgStruct.attachments)
                warning('attachment file name(s) is/are not a character vector or cell array of char vectors\nCould not send attachment(s)!')
                msgStruct.attachments = [];
            end
            if iscell(msgStruct.attachments) && ~isempty(msgStruct.attachments)
                for iFilepath = 1:numel(msgStruct.attachments)
                    if ~ischar(msgStruct.attachments{iFilepath})
                        warning('attachment file name(s) is/are not a character vector or cell array of char vectors\nCould not send attachment(s)!')
                        msgStruct.attachments = [];
                    end
                end
            end
            if ~isempty(msgStruct.attachments)
                if ~isfile(msgStruct.attachments)
                    warning('file(s) to be attached does not exist in stated location(s)\nCould not send attachment(s)!')
                    msgStruct.attachments = [];
                end
            end
        else
            msgStruct.attachments = [];
        end
        
        %basic checks for the sender struct contents
        if ~isstruct(senderStruct)
            error('sender information must be contained in a struct!')
        end
        if ~isfield(senderStruct, {'address', 'password', 'hostSmtp', 'hostPort'})
            error('missing/incorrect fields in sender struct')
        end
        if ~ischar(senderStruct.address)
            error('sender email address is not a character vector')
        end
        if isempty(senderStruct.address)
            error('sender email address is empty')
        end
        atPos = strfind(senderStruct.address,'@');
        if numel(atPos) ~= 1
            error('invalid sender email address character vector?');
        end
        dotPos = strfind(senderStruct.address,'.');
        if ~any(dotPos > atPos)
            error('invalid sender email address character vector?');
        end
        if isempty(senderStruct.password)
            error('sender password is empty');
        end
        if ~ischar(senderStruct.password)
            error('sender password must be a character vector');
        end
        if isempty(senderStruct.hostSmtp)
            error('sender SMTP host url is empty');
        end
        if ~ischar(senderStruct.hostSmtp)
            error('sender SMTP host must be a character vector');
        end
        if isempty(senderStruct.portSmtp)
            error('sender SMTP port is empty');
        end
        if ~ischar(senderStruct.portSmtp)
            error('sender SMTP port must be a character vector');
        end
    else
        error('error: invalid number of input arguments');
    end

    setpref( 'Internet', 'E_mail', senderStruct.address);
    setpref( 'Internet', 'SMTP_Server', senderStruct.hostSmtp );
    setpref( 'Internet', 'SMTP_Username', senderStruct.address );
    setpref( 'Internet', 'SMTP_Password', senderStruct.password );
    
    props = java.lang.System.getProperties;
    props.setProperty( 'mail.smtp.user', senderStruct.address );
    props.setProperty( 'mail.smtp.from', senderStruct.address );
    props.setProperty( 'mail.smtp.host', senderStruct.hostSmtp );
    props.setProperty( 'mail.smtp.port', senderStruct.portSmtp );
    props.setProperty( 'mail.smtp.starttls.enable', 'true' );
    props.setProperty( 'mail.smtp.debug', 'true' );
    props.setProperty( 'mail.smtp.auth', 'true' );
    props.setProperty( 'mail.smtp.socketFactory.port', senderStruct.portSmtp );
    props.setProperty( 'mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory' );
    props.setProperty( 'mail.smtp.socketFactory.fallback', 'false' );
    
    %actually send e-mail
    sendmail(msgStruct.address, msgStruct.subject, msgStruct.textbody, msgStruct.attachments);
    
catch xcp
    errStr = getReport(xcp);
    fprintf(errStr);
    if ~isempty(errorFunHandle)
        errCallbackOutput = errorFunHandle(errorFunParams);
    end
    return %do not crash this program
end
return
end