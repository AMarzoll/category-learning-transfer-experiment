function [adaptedTextBounds_pix, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, string, outerRect_pix, xMargin_pix, yMargin_pix, wallOfTextFlag)
%This function ensures that text does not exceed its containing rectangle.
%It returns an appropriate font size in newFontSize and the previous one as
%oldFontSize. You still have to change the font size yourself using these
%values. It also returns a rectangle indicating the text block's size in
%adaptedTextBounds_pix. You can also specify a margin to be kept from the
%rectangle's borders with xMargin_pix/yMargin_pix. For large blocks of
%text, where the size estimation seems to be wonky, you'd probably want to
%choose large margins to be on the safe side. For large bodies of text,
%e.g. instructions, set the wallOfTextFlag to true to enable automatic line
%breaks. Warning: This will also override your graphics buffer contents.

[x_pix, y_pix] = Screen('WindowSize', winPtr);
oldFontSize = Screen('TextSize', winPtr);
curFontSize = oldFontSize;
grayColor = GrayIndex(winPtr);
blackColor = BlackIndex(winPtr);
while true %decrease font size until text fits
    if ~wallOfTextFlag
        adaptedTextBounds_pix = Screen('TextBounds', winPtr, string);
    else
        %side-effect warning: this will have to overwrite graphics buffer contents
        [~, ~, adaptedTextBounds_pix] = DrawFormattedText(winPtr, string, 'center', 'center', blackColor, ...
            80, false, false, 1.5, 0, outerRect_pix);
        Screen('FillRect', winPtr, grayColor, [0 0 x_pix y_pix]);
    end
    isTextWithinBounds = adaptedTextBounds_pix(3) <= (outerRect_pix(3)-outerRect_pix(1)) - xMargin_pix && ...
        adaptedTextBounds_pix(4) <= (outerRect_pix(4)-outerRect_pix(2)) - yMargin_pix;
    if isTextWithinBounds
        break;
    else
        curFontSize = curFontSize - 1;
        Screen('TextSize', winPtr, curFontSize);
    end
end
newFontSize = curFontSize;
Screen('TextSize', winPtr, oldFontSize);
end