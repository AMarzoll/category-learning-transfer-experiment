function [exp, catTaskFlag, odTaskFlag] = defineExperimentParameters(dis, subj, cfgFileUtils)
%Defines parameters for the task procedure, stimulus space and timing
%values and saves them in the 'exp' struct

%% common parameters

    exp.fixCrossSize_deg      = 0.1; %diameter, not just arm length
    exp.fixCrossSurround_deg  = 0.2;
    exp.fixCrossLineWidth_pix = 1;
    exp.fixCrossColor         = [0.0 0.0 0.0];
    exp.fixCrossColor_cue     = [0.6 0.6 1.0];
    exp.feedbackTextSize      = 30;
    exp.posFeedbackChar       = '263a'; %smiling face
    exp.negFeedbackChar       = '00d7'; %x-type cross
    exp.posFeedbackColor      = [0.0 1.0 0.0];
    exp.negFeedbackColor      = [0.6 0.0 0.0];
    
    exp.odSessions            = [1, 9, 17];
    exp.catSessions           = [2:8, 10:16];
    exp.lastSession           = 17;
    exp.phaseTwoStart         = 10;

%% determine parameter definitions for this session
    
    thisSession = subj.nSessionsCompleted + 1;
    if ismember(thisSession, exp.odSessions)
        odTaskFlag = true;
    else
        odTaskFlag = false;
    end
    
    if ismember(thisSession, exp.catSessions)
        catTaskFlag = true;
        if thisSession >= exp.phaseTwoStart
            exp.curPhase = 2;
        else
            exp.curPhase = 1;
        end
    else
        catTaskFlag = false;
    end
    
%% categorization task parameters

    if catTaskFlag
        %general task parameters
        exp.cat.nBlocks         = 8;
        exp.cat.nTrialsPerBlock = 100;

        %staircase parameters
        exp.cat.nDown        = 3;
        exp.cat.nUp          = 1;
        exp.cat.initStep     = 1;
        exp.cat.stepSizePerc = 0.20;
        exp.cat.nSteps       = 100;

        %stimulus space
        exp.cat.catRadius           =  cfgFileUtils.readParam('exp.cat.catRadius', 'num');
        exp.cat.catCenter           = [cfgFileUtils.readParam('exp.cat.catCenter1', 'num'), ...
                                       cfgFileUtils.readParam('exp.cat.catCenter2', 'num'), ...
                                       cfgFileUtils.readParam('exp.cat.catCenter3', 'num'), ...
                                       cfgFileUtils.readParam('exp.cat.catCenter4', 'num')];
        if exp.curPhase == 2
            exp.cat.catCenter = 1 - exp.cat.catCenter; %shift center to opposite "corner"
        end
        
        if exp.curPhase == 1
            exp.cat.catBoundaryNormVec  = [cfgFileUtils.readParam('exp.cat.catBoundaryNormVec1_1', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec1_2', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec1_3', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec1_4', 'num')];
        elseif exp.curPhase == 2
            exp.cat.catBoundaryNormVec  = [cfgFileUtils.readParam('exp.cat.catBoundaryNormVec2_1', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec2_2', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec2_3', 'num'), ...
                                           cfgFileUtils.readParam('exp.cat.catBoundaryNormVec2_4', 'num')];
        end

        %supplemental parameters
        if exp.curPhase == 1
            exp.cat.keyCatA = KbName(cfgFileUtils.readParam('exp.cat.keyCatA_1', 'char'));
            exp.cat.keyCatB = KbName(cfgFileUtils.readParam('exp.cat.keyCatB_1', 'char'));
        elseif exp.curPhase == 2
            exp.cat.keyCatA = KbName(cfgFileUtils.readParam('exp.cat.keyCatA_2', 'char'));
            exp.cat.keyCatB = KbName(cfgFileUtils.readParam('exp.cat.keyCatB_2', 'char'));
        end
        
        exp.cat.key2cat = containers.Map([exp.cat.keyCatA, exp.cat.keyCatB], [0, 1]); %maps responses to numbers

        %timing
        %round to next highest time that's an integer number of frames
        exp.cat.preCueDur_sec       = ceil(0.500 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.cat.cueDur_sec          = ceil(0.100 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.cat.postCueDur_sec      = ceil(0.300 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.cat.stimDur_sec         = ceil(1.000 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.cat.backwardMaskDur_sec = ceil(0.150 / dis.frameDur_sec) * dis.frameDur_sec; %from 0.050
        exp.cat.responseDur_sec     = ceil(5.000 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.cat.feedbackDur_sec     = ceil(0.300 / dis.frameDur_sec) * dis.frameDur_sec;
    end
%% orientation discrimination task parameters

    if odTaskFlag
        %general task parameters
        exp.od.nBlocks          = 4;
        exp.od.nTrialsPerBlock  = 50;
        exp.od.firstOri         = cfgFileUtils.readParam('exp.od.firstOri', 'num');

        %staircase parameters
        exp.od.nDown        = 4; %like Schoups, Vogels & Orban, 1995 (J Physiol)
        exp.od.nUp          = 1;
        exp.od.initStepIdx  = 8;
        exp.od.initStepVal  = 7; %like Schoups, Vogels & Orban, 1995 (J Physiol)
        exp.od.nSteps       = 50;
        exp.od.stepSizePerc = 0.2; %like Schoups, Vogels & Orban, 1995 (J Physiol)

        %stimulus space
        %oris and sf's are based on parameters from categorization task
        exp.od.boundaryOris = CatStimulus.calculateCenterLinearOris(cfgFileUtils);
        exp.od.sfs          = CatStimulus.calculateCenterLinearSFs(cfgFileUtils);

        %supplemental parameters
        if ispc
            exp.od.ccwKey  = KbName('left');
            exp.od.cwKey   = KbName('right');
        elseif ismac
            exp.od.ccwKey  = KbName('LeftArrow');
            exp.od.cwKey   = KbName('RightArrow');
        end
        exp.od.key2ori = containers.Map([exp.od.ccwKey, exp.od.cwKey], [0, 1]); %maps responses to numbers

        %timing
        exp.od.preCueDur_sec        = ceil(0.500 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.cueDur_sec           = ceil(0.100 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.postCueDur_sec       = ceil(0.300 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.stimDur_sec          = ceil(0.200 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.backwardMaskDur_sec  = ceil(0.050 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.responseDur_sec      = ceil(5.000 / dis.frameDur_sec) * dis.frameDur_sec;
        exp.od.feedbackDur_sec      = ceil(0.300 / dis.frameDur_sec) * dis.frameDur_sec;
    end

end