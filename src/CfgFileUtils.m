classdef CfgFileUtils
    %Handles obfuscating/decoding of and reading/writing parameters from and to a
    %config file for individualized or otherwise non-hardcoded parameters.
    %Each line in the plain-text config file represents a parameter name and its value. This can be read
    %as char array or as a numerical value. Plain-text files must be
    %obfuscated using a static member function
    
    properties (SetAccess = private)
        userDir
        expSubDir
        cfgFileName = 'exp.cfg';
        cfgFilePath
    end
    
    
    
    methods
        function obj = CfgFileUtils(machine)
            if ispc
                obj.userDir = getenv('USERPROFILE');
            else
                obj.userDir = getenv('HOME');
            end
            obj.expSubDir = machine.expSubDir;
            obj.cfgFilePath = fullfile(obj.userDir, obj.expSubDir, obj.cfgFileName);
            
            if ~exist(obj.cfgFilePath, 'file')
                error(['Config file ''exp.cfg'' not found in correct location! Please ensure it''s in a subfolder ' ...
                       'of your operating system''s home folder with the name ''visual_experiment''. Please contact ' ...
                       'experimenter if problem persists.']);
            end
        end
        
        
        
        function out = doesParamExist(obj, paramName)
            [~, idx] = findParam(obj, paramName);
            if ~isempty(idx) && (numel(idx) == 1)
                out = true;
            elseif ~isempty(idx)
                warning('Parameter name ambiguous, more than one result found')
                out = false;
            else
                out = false;
            end
        end
        
        
        
        function out = readParam(obj, paramName, type)
            %type must be 'num' or 'char'
            [parsedText, idx] = obj.findParam(paramName);
            if isempty(idx) || (numel(idx) > 1)
                error('Parameter not found or more than one found');
            end
            out = parsedText{1,2}{idx};
            if strcmp(type, 'num')
                out = str2num(out);
            elseif strcmp(type, 'char')
                return;
            else
                error('Invalid type');
            end
        end
        
        
        
        function writeParam(obj, paramName, val)
            %determine if variable already exists
            [parsedText, idx] = obj.findParam(paramName);
            if numel(idx) > 1
                error('More than one parameter found')
            end
                %if yes, overwrite existing value
                if ~isempty(idx)
                    parsedText{1,2}{idx} = num2str(val);
                %if no, create entry and write value
                else
                    parsedText{1,1}{end+1} = paramName;
                    parsedText{1,2}{end+1} = num2str(val);
                end
            writecell([parsedText{1,1} parsedText{1,2}], obj.cfgFilePath, 'Delimiter', '\t', 'FileType', 'text');
            
            %convert from cell to chars and re-encode
            charArray = '';
            for iRow = 1:numel(parsedText{1,1})
                for iCol = 1:2
                    charArray = [charArray parsedText{iCol}{iRow}];
                    if iCol == 1
                        charArray = [charArray char(9)]; %tab
                    end
                end
                charArray = [charArray char(10)]; %line break
            end
            encoded = dec2hex(charArray, 2);
            
            %write to file
            fPtr = fopen(obj.cfgFilePath, 'w');
            fwrite(fPtr, encoded);
            fclose(fPtr);
        end
    end
    
    
    
    methods (Access = private)
        function [parsedText, idx] = findParam(obj, paramName)
            if ~ischar(paramName)
                error('paramName must be a character array')
            end
            
            %read text
            fPtr = fopen(obj.cfgFilePath);
            encoded = fread(fPtr);
            fclose(fPtr);
            
            %decode text
            encoded = reshape(encoded, [], 2);
            encoded = hex2dec(encoded);
            decoded = char(encoded);
            
            %scan for parameter
            parsedText = textscan(decoded, '%s %s', 'Delimiter', '\t');
            containsParam = cellfun(@(x)strfind(x, paramName), parsedText{1,1}, 'UniformOutput', false);
            idx = find(~cellfun('isempty', containsParam));
        end
    end
    
    
    
    methods (Static)
        function convertConfigFile(textOrFileToEncode, outputFilePath)
            
            %read text from file if necessary
            if ischar(textOrFileToEncode) && ~isfile(textOrFileToEncode)
                plainText = textOrFileToEncode;
            elseif ischar(textOrFileToEncode) && isfile(textOrFileToEncode)
                fPtr = fopen(textOrFileToEncode);
                plainText = fread(fPtr);
                fclose(fPtr);
            else
                error('Plain text for config file must be char array or a file containing text')
            end
            
            %convert to hexadecimal numbers and save to file
            encoded = dec2hex(plainText, 2);
            fPtr = fopen(outputFilePath, 'w');
            fwrite(fPtr, encoded);
            fclose(fPtr);
        end
        
        
        
        function decodeConfigFile(textFileToDecode, outputFilePath)
            %read text
            fPtr = fopen(textFileToDecode);
            encoded = fread(fPtr);
            fclose(fPtr);
            
            %decode text
            encoded = reshape(encoded, [], 2);
            encoded = hex2dec(encoded);
            decoded = char(encoded);
            
            fPtr = fopen(outputFilePath, 'w');
            fwrite(fPtr, decoded);
            fclose(fPtr);
        end
    end
end