function pixPerCm = pixPerCmEstimation(winPtr, kbIdx)
%This function enables estimation of screen size using a credit card (see Li et al., 2020, SciRep).
%It requires a pointer to an opened Psychtoolbox window. It depends on the
%'Button' class and the 'adaptTextBoundsToRect' function, but can be easily
%plugged into one of your experiments

%get some screen info
[x_pix, y_pix] = Screen('WindowSize', winPtr);
aspectRatio = x_pix/y_pix;
halfFrameDur_sec = 0.5 * (1/FrameRate(winPtr));

%parameter configuration
sliderButtonRadius_norm     = [0.02/aspectRatio 0.02]; %normalized parameters as percentage of entire screen height/width
sliderBarRect_norm          = [0.15 0.15 0.85 0.17];
doneButtonCenter_norm       = [0.80 0.75];
backButtonCenter_norm       = [0.80 0.60];
initialComparatorSize_norm  = 0.5;
buttonOutlineParam          = 2;

sliderBarColor              = [0.65 0.65 0.65];
sliderButtonColor           = [0.80 0.80 0.80];
outlineColor                = [0.40 0.40 0.40];
comparatorColor             = [0.30 0.80 0.30];

%credit card
creditCardSize_cm = [8.560 5.398];
creditCardRatio = creditCardSize_cm(1) / creditCardSize_cm(2);

%strings
instructionStr = ['-- SCREEN SIZE MEASUREMENT --\nINSTRUCTIONS\n\nBefore we can start the experiment, we need measure the physical size of your computer screen. ', ...
                  'For that, we need an object of a known size. The most convenient object is a credit/debit card. ', ...
                  'Many similar ID cards are of the same size (e.g. a Brown University ID card or many driving licenses (USA/EU)). ' ...
                  'Please locate such a card before you continue.\n\n', ...
                  'On the following screen, there will be a green rectangle in the bottom left of the screen. Please place your card ', ...
                  'on the bottom left corner of the screen display area. Using the slider at the top, adjust the size of the green ', ...
                  'rectangle until it matches the size of your card perfectly. When you are done, press the button in the bottom ', ...
                  'right of the screen.\n\nWhen you are ready, press any key to continue.'];
doneButtonStr   = 'Done!';
backButtonStr   = 'Back to Instructions';
              
%convenience lambda
norm2pixRect = @(in) round([in(1)*x_pix, in(2)*y_pix, in(3)*x_pix, in(4)*y_pix]);

%calculate values for on-screen presentation
sliderBarRect_pix           = norm2pixRect(sliderBarRect_norm);
sliderButtonRect_pix        = norm2pixRect([sliderBarRect_norm(1)+(sliderBarRect_norm(3)-sliderBarRect_norm(1))/2 - sliderButtonRadius_norm(1) ...
                                            sliderBarRect_norm(2)+(sliderBarRect_norm(4)-sliderBarRect_norm(2))/2 - sliderButtonRadius_norm(2) ...
                                            sliderBarRect_norm(1)+(sliderBarRect_norm(3)-sliderBarRect_norm(1))/2 + sliderButtonRadius_norm(1) ...
                                            sliderBarRect_norm(2)+(sliderBarRect_norm(4)-sliderBarRect_norm(2))/2 + sliderButtonRadius_norm(2)]);
sliderButtonRadius_pix      = (sliderButtonRect_pix(3) - sliderButtonRect_pix(1))/2;
sliderState                 = 0.5; %normalized position along the slider itself
sliderEdges_pix             = [sliderBarRect_pix(1) sliderBarRect_pix(3)];
sliderRange_pix             = sliderBarRect_pix(3) - sliderBarRect_pix(1);
sliderButtonColor_mouseover = sliderButtonColor * 1.25;

%define other buttons
doneButton = Button(doneButtonCenter_norm, doneButtonStr, [0 0 x_pix y_pix]);
backButton = Button(backButtonCenter_norm, backButtonStr, [0 0 x_pix y_pix]);

%presentation preparation
ShowCursor(1);
isLastPressed = false;
isCurrentlyMoving = false;
[lastMouseX, ~] = GetMouse();
vbl = GetSecs();
showInstructions = true;
doneFlag = false;

%display GUI until confirmation button is pressed
while true
    
    %TODO (optional): choice between credit card or other object of known size
    
    %instruction screen
    if showInstructions
        [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, instructionStr, [0 0 x_pix y_pix], 300, 100, true);
        Screen('TextSize', winPtr, newFontSize);
        DrawFormattedText(winPtr, instructionStr, 'center', 'center', [0 0 0], 80, false, false, 1.5);
        Screen('Flip', winPtr);
        Screen('TextSize', winPtr, oldFontSize);
        showInstructions = false;
        WaitSecs(0.5);
        KbStrokeWait(kbIdx);
    end
    
    %poll mouse state
    [mouseX, mouseY, mouseButtons] = GetMouse();
    isMouseoverSlider = IsInRect(mouseX, mouseY, sliderButtonRect_pix);
    isPressed = mouseButtons(1);
    
    %highlight the slider button when appropriate
    if isMouseoverSlider || isCurrentlyMoving
        sliderCircleColor_current = sliderButtonColor_mouseover;
    else
        sliderCircleColor_current = sliderButtonColor;
    end
    
    %move the slider button when dragged by the mouse and update the
    %size of the comparator rectangle
    if (isMouseoverSlider || isCurrentlyMoving) && isPressed && isLastPressed
        isCurrentlyMoving = true;
        deltaX = lastMouseX - mouseX;
        sliderButtonRect_pix(1) = sliderButtonRect_pix(1) - deltaX;
        sliderButtonRect_pix(1) = max([sliderBarRect_pix(1)-sliderButtonRadius_pix, sliderButtonRect_pix(1)]);
        sliderButtonRect_pix(1) = min([sliderBarRect_pix(3)-sliderButtonRadius_pix, sliderButtonRect_pix(1)]);
        sliderButtonRect_pix(3) = sliderButtonRect_pix(1) + sliderButtonRadius_pix*2;
        
        sliderButtonCtr_pix = sliderButtonRect_pix(1) + sliderButtonRadius_pix;
        sliderState = (sliderButtonCtr_pix - sliderEdges_pix(1)) / sliderRange_pix;
    else
        isCurrentlyMoving = false;
    end
    
    %draw slider and slider button
    Screen('FillRect', winPtr, sliderBarColor, sliderBarRect_pix);
    Screen('FrameRect', winPtr, outlineColor, sliderBarRect_pix, buttonOutlineParam);
    Screen('FillOval', winPtr, sliderCircleColor_current, sliderButtonRect_pix);
    Screen('FrameOval', winPtr, outlineColor, sliderButtonRect_pix, buttonOutlineParam);
    
    %handle confirmation button  
    isDoneButtonPressed = doneButton.drawButton(winPtr);
    if isDoneButtonPressed
        doneFlag = true;
    end
    
    %handle back button
    isBackButtonPressed = backButton.drawButton(winPtr);
    if isBackButtonPressed
        showInstructions = true;
    end

    %draw area for comparison object
    comparatorRect_norm  = [0, ...
                            1-(initialComparatorSize_norm*aspectRatio/creditCardRatio*sliderState), ...
                            initialComparatorSize_norm*sliderState, ...
                            1];
    comparatorRect_pix   = norm2pixRect(comparatorRect_norm);
    Screen('FillRect', winPtr, comparatorColor, comparatorRect_pix);

    %render everything
    vbl = Screen('Flip', winPtr, vbl+halfFrameDur_sec);

    %remember mouse state
    lastMouseX = mouseX;
    isLastPressed = isPressed;
    
    %leave loop when done (after Flip to avoid leftovers in buffer)
    if doneFlag
        break;
    end
end

%calculate output
pixPerCm_x = comparatorRect_pix(3) / creditCardSize_cm(1);
pixPerCm_y = (comparatorRect_pix(4) - comparatorRect_pix(2)) / creditCardSize_cm(2);
if pixPerCm_x/pixPerCm_y > 1.02 || pixPerCm_x/pixPerCm_y < 1/1.02
    warning('Unusually large deviation in dpi for x- and y-axes!');
end
pixPerCm = mean([pixPerCm_x, pixPerCm_y]);
HideCursor();
end
