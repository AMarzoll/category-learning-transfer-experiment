function kbIdx = keyboardTest(winPtr, dis)
%Determines actual keyboard to use in case there are several candidates

keyboardIndices = GetKeyboardIndices();
    if numel(keyboardIndices) > 1
        %draw instruction screen
        instructionStr = ['More than one keyboard has been detected, which can lead to problems in the experiment. Using the keyboard '...
                          'in front of you, please press the space bar repeatedly.'];
        [~, oldFontSize, newFontSize] = adaptTextBoundsToRect(winPtr, instructionStr, dis.screenRect, 200, 100, true);
        Screen('TextSize', winPtr, newFontSize);
        DrawFormattedText(winPtr, instructionStr, 'center', 'center', dis.textColor, 80, false, false, 1.5);
        Screen('TextSize', winPtr, oldFontSize);
        Screen('Flip', winPtr);
        %cycle through all keyboards
        while true
            for iKb = 1:numel(keyboardIndices)
                %record key presses..if at least one has been found, select
                %that keyboard
                [~, keyCode, ~] = KbWait(keyboardIndices(iKb), [], GetSecs()+1);
                if any(keyCode)
                    kbIdx = keyboardIndices(iKb);
                    return;
                end
            end
        end
    elseif numel(keyboardIndices) == 0
        error('Error: no keyboard could be found using GetKeyboardIndices()!')
    else
        %default case: just one keyboard found
        kbIdx = keyboardIndices;
    end
end