classdef OdStimulus
    %handles orientation discrimination task stimulus image generation

    properties (SetAccess = private)
        
        ori     = NaN;
        sf      = NaN;
        sf_pix  = NaN;
        phase   = NaN;
        
        sz_pix  = NaN;
        c       = 0.3; %contrast, must be mutable for example stimulus visibility
        s_pix   = NaN;
        
    end
    
    properties (Constant)
        
        sz_deg  = 5; %canvas size
        c_noise = 0.3;
        s       = 0.3; %standard deviation of Gaussian envelope
        
    end
    
    methods
        
        function obj = OdStimulus(ori, sf, dis)
            if isempty(ori) && isempty(sf) && isempty(dis) %for memory pre-allocation
                return;
            end
            
            obj.ori    = ori;
            obj.sf     = sf;
            obj.sf_pix = 1/dis.deg2pix(1/obj.sf);
            obj.sz_pix = round(dis.deg2pix(obj.sz_deg));
            obj.phase  = 2.0*pi*rand;
            obj.s_pix  = dis.deg2pix(obj.s);
        end
        
        function img = generateImage(obj)
            %generate Cartesian coordinate system
            if mod(obj.sz_pix, 2) == 0
                [x,y] = meshgrid(-obj.sz_pix/2+1:obj.sz_pix/2, obj.sz_pix/2:-1:-obj.sz_pix/2+1);
            else
                [x,y] = meshgrid(-obj.sz_pix/2+0.5:obj.sz_pix/2-0.5, obj.sz_pix/2-0.5:-1:-obj.sz_pix/2+0.5);
            end
            
            img = 127*(1.0 + (obj.c*(sin(2.0*pi*obj.sf_pix*(y*sind(obj.ori) +  x*cosd(obj.ori)) + obj.phase))) .* ...
                  exp(-(x.^2.0+y.^2.0)./(2.0*obj.s_pix^2.0)));
            img = img ./ 255;
        end
        
        function img = generateNoise(obj)
            img = 0.5 + randn(obj.sz_pix, obj.sz_pix) * obj.c_noise;
        end
        
        function obj = setGaborContrast(obj, cNew)
            obj.c = cNew;
        end
    end
end

