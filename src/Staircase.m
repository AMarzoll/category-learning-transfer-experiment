classdef (Abstract) Staircase
    %Abstract class for common staircase generation facilities
    
    properties (SetAccess = protected)
        nUp
        nDown
        
        firstWrongResp = false; %flag for 1-down-x-up procedure before the first incorrect response
        curTrial = 0;
        curStepIdx
        
        nSteps
        steps
        streak = 0;
        stepSizePerc
        nTrialsPerBlock
    end
    
    methods
        function obj = Staircase(paramStruct)
            if isempty(paramStruct)
                return;
            end
            obj.nUp             = paramStruct.nUp;
            obj.nDown           = paramStruct.nDown;
            obj.nSteps          = paramStruct.nSteps;
            obj.stepSizePerc    = paramStruct.stepSizePerc;
            obj.nTrialsPerBlock = paramStruct.nTrialsPerBlock;
        end
        
        function obj = nextTrial(obj, isLastCorrect)
            %figure out current streak (positive integer for correct answers,
            %negative for wrong answers)
            obj.curTrial = obj.curTrial + 1;
            if obj.curTrial ~= 1
                if isLastCorrect
                    if obj.streak > 0
                        obj.streak = obj.streak + 1;
                    else
                        obj.streak = 1;
                    end
                else
                    obj.firstWrongResp = true;
                    if obj.streak >= 0
                        obj.streak = -1;
                    else
                        obj.streak = obj.streak - 1;
                    end
                end
            end
            
            %1-down before the first mistake is committed (skip easier
            %trials)
            if obj.firstWrongResp
                curNDown = obj.nDown;
            else
                curNDown = 1;
            end
            
            %change difficulty if sufficient number of steps is reached
            if obj.streak == curNDown
                obj.curStepIdx = min([obj.curStepIdx + 1, obj.nSteps]); %'down' means more difficult, so INCREASE index
                obj.streak = 0;
            elseif obj.streak == -obj.nUp
                obj.curStepIdx = max([obj.curStepIdx - 1, 1]); %and vice versa
                obj.streak = 0;
            end
        end
        
        
        
        function obj = setCurTrial(obj, newCurTrial)
            obj.curTrial = newCurTrial;
        end
        
    end
    
    methods (Static)
        
        function [thr, nTotalReversals] = computeThreshold(nReversals, vals)
            %compute the the threshold for this block, using the geometric mean 
            %of the last N reversals
            [peaks, peakLocs] = findpeaks(vals);
            [valleys, valleyLocs] = findpeaks(-vals);
            valleys = valleys * (-1);
            allLocs = [peakLocs valleyLocs];
            reversals = [peaks, valleys];
            [~, sortIdx] = sort(allLocs);
            reversals = reversals(sortIdx);
            nTotalReversals = numel(reversals);
            if numel(reversals) >= nReversals
                thr = geomean(reversals(end-nReversals+1:end));
            elseif isempty(reversals)
                warning('No reversals found. Performance likely at chance level.')
                thr = NaN;
            else
                warning('Fewer reversals than required for threshold calculation')
                thr = geomean(reversals);
            end
        end
        
    end
end

