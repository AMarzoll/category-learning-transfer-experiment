classdef CatExperiment < ExperimentLoop
    %Experiment loop for the category learning task
    
    properties (Constant)
        nFlipsPerTrial = 6;
        
    end

    properties (SetAccess = private)
        titleStr
        instructionStr
    end
    
    methods
        
        function obj = CatExperiment(winPtr, machine, subj, dis, exp, saveManager, startBlock)
            obj = obj@ExperimentLoop(winPtr, machine, subj, dis, exp, saveManager, startBlock, 'cat');
            
            obj.titleStr = sprintf('-- CATEGORY LEARNING TASK - PHASE %u --', exp.curPhase);
            obj.instructionStr = sprintf(...
               ['-- CATEGORY LEARNING TASK - PHASE %u --\nINSTRUCTIONS\n\nIn the following task, you are asked to divide visual stimuli into two categories, labeled ' ...
                '''%c'' and ''%c''. These stimuli are not like everyday images of objects, but rather abstract. It is your task to ' ...
                'figure out the rules according to which a given stimulus image belongs to either of these two categories. In the ' ...
                'beginning, these rules will be completely unknown to you, so do not be worried by making a lot of mistakes. You ' ...
                'will be informed after each trial if your response was correct or not. Please respond as accurately as possible ' ...
                'within five seconds after the image has disappeared. You will hear a beeping sound if you did not respond in time. ' ...
                'The two response keys on your keyboard are ''%c'' and ''%c''. Please keep your eyes fixated on the center of the ' ...
                'screen, indicated by a cross.\n\nWhen you are ready, press any key to continue.'], ...
                exp.curPhase, ...
                osxCharRemap(exp.cat.keyCatA), ...
                osxCharRemap(exp.cat.keyCatB), ...
                osxCharRemap(exp.cat.keyCatA), ...
                osxCharRemap(exp.cat.keyCatB));
        end
        
        
        
        function [res, exitFlag] = run(obj, res)
            showInstructions = true;
            HideCursor();
            exitFlag = false;
            
            %KbQueue preparation
            validKeys = [obj.exp.cat.keyCatA, obj.exp.cat.keyCatB];
            obj.prepareResponseCollection(validKeys);
            
            %title screen
            obj.showTitleScreen(obj.titleStr);
            
            for iBlock = obj.startBlock:obj.exp.cat.nBlocks
                
                %new staircase
                staircase = CatStaircase(obj.exp);
                isLastCorrect = true; %default value for first trial
                
                %show countdown
                if iBlock ~= obj.startBlock
                    obj.showCountdown();
                end
                
                for iTrial = 1:obj.exp.cat.nTrialsPerBlock
                    
                    iFlip = 1; %for timestamp indexing
                    
                    %display instructions
                    if showInstructions
                        obj.showInstructionScreen(obj.instructionStr);
                        showInstructions = false;
                    end
                    
                    %compute trial parameters
                    [staircase, stimParams_norm] = staircase.nextTrial(isLastCorrect);
                    
                    %generate stimulus and noise images
                    stimulus = CatStimulus(stimParams_norm, obj.dis);
                    stimulusImage = stimulus.generateImage();
                    noiseImage = stimulus.generateNoise();
                    stimParams_properUnits = stimulus.getStimProperties();
                    res.cat.stimuli(iBlock, iTrial) = stimulus;
                    
                    %save stimulus properties in result struct
                    res.cat.trialTable(iBlock, iTrial, 1) = iTrial;
                    res.cat.trialTable(iBlock, iTrial, 2) = staircase.getCurCat();
                    res.cat.trialTable(iBlock, iTrial, 3) = staircase.getCurBoundaryDist();
                    res.cat.trialTable(iBlock, iTrial, 4:7) = stimParams_norm;
                    res.cat.trialTable(iBlock, iTrial, 8:11) = stimParams_properUnits;
                    
                    %get vbl
                    vbl = Screen('Flip', obj.winPtr);
                    
                    %preCue
                    obj.drawFixationCross(false);
                    flipWrapper(obj.dis.frameDur_sec);
                    
                    %cue
                    obj.drawFixationCross(true);
                    flipWrapper(obj.exp.cat.preCueDur_sec);
                    
                    %postCue
                    obj.drawFixationCross(false);
                    flipWrapper(obj.exp.cat.cueDur_sec);
                    
                    %stimulus
                    texPtr = Screen('MakeTexture', obj.winPtr, stimulusImage, [], [], obj.dis.floatPrecision);
                    Screen('DrawTexture', obj.winPtr, texPtr);
                    obj.drawFixationCross(false);
                    flipWrapper(obj.exp.cat.postCueDur_sec);
                    Screen('Close', texPtr);
                    
                    %record button presses from stimulus onset
                    KbQueueStart(obj.machine.kbIdx);
                    respStartTime_sec = vbl;
                    
                    %backward masking
                    texPtr = Screen('MakeTexture', obj.winPtr, noiseImage, [], [], obj.dis.floatPrecision);
                    Screen('DrawTexture', obj.winPtr, texPtr);
                    flipWrapper(obj.exp.cat.stimDur_sec);
                    Screen('Close', texPtr);
                    
                    %response period
                    obj.drawFixationCross(false);
                    flipWrapper(obj.exp.cat.backwardMaskDur_sec);
                    
                    %record response
                    correctRespLambda = @(key) obj.exp.cat.key2cat(key) == staircase.getCurCat();
                    [respKey, isCorrect, isMiss, respTime_sec] = obj.collectResponse(respStartTime_sec, correctRespLambda);
                    
                    %make entries in result struct
                    if ~isMiss
                        res.cat.trialTable(iBlock, iTrial, 12) = respKey == obj.exp.cat.keyCatB; %0 for category A, 1 for category B
                    else
                        res.cat.trialTable(iBlock, iTrial, 12) = NaN; %NaN for a missed trial
                    end
                    res.cat.trialTable(iBlock, iTrial, 13) = isCorrect;
                    res.cat.trialTable(iBlock, iTrial, 14) = isMiss;
                    res.cat.trialTable(iBlock, iTrial, 15) = respTime_sec;
                    
                    %show feedback
                    obj.showFeedback(isCorrect, isMiss);
                    
                    %suspend experiment if there are three misses in a row
                    %(but not if it's the last trial of the block)
                    if (iTrial >= 3) && all(res.cat.trialTable(iBlock, iTrial-2:iTrial, 14) == true) && (iTrial ~= obj.exp.cat.nTrialsPerBlock)
                        [exitFlag, instructionFlag] = obj.showPauseScreen(true, false);
                        if instructionFlag
                            showInstructions = true;
                        end
                        if exitFlag
                            return;
                        end
                    end
                    
                    isLastCorrect = isCorrect; %for next trial
                end %end of a block
                
                %save staircase object - just in case
                res.cat.staircases(iBlock) = staircase;
                
                %do block summary statistics
                res.cat.blockTable(iBlock, 1) = iBlock;
                res.cat.blockTable(iBlock, 2) = geomean(staircase.getAllBoundaryDistances());
                res.cat.blockTable(iBlock, 3) = staircase.computeThreshold(res.cat.nReversals);
                res.cat.blockTable(iBlock, 4) = sum(res.cat.trialTable(iBlock, :, 14), 'omitnan');
                res.cat.blockTable(iBlock, 5) = mean(res.cat.trialTable(iBlock, :, 15), 'omitnan');
                res.cat.blockTable(iBlock, 6) = log10(mean(res.cat.trialTable(iBlock, :, 12), 'omitnan') / 0.5);
                
                %do autosave
                obj.saveManager.createAutosaveFile(obj.machine, obj.subj, obj.dis, obj.exp, res);
                
                %show pause screen (except on last block)
                if iBlock ~= obj.exp.cat.nBlocks
                    [exitFlag, instructionFlag] = obj.showPauseScreen(false, true);
                    if exitFlag
                        return;
                    end
                    if instructionFlag
                        showInstructions = true;
                    end
                end
                
                Screen('FillRect', obj.winPtr, [0.5 0.5 0.5], [0 0 obj.dis.screenRect(3) obj.dis.screenRect(4)]);
                Screen('Flip', obj.winPtr);
                
            end
            
            KbQueueRelease(obj.machine.kbIdx);
            
            %subroutine for reducing visual clutter
            function flipWrapper(time)
                [vbl, res.cat.sot(iBlock, iTrial, iFlip), res.cat.ft(iBlock, iTrial, iFlip), res.cat.ms(iBlock, iTrial, iFlip)] = ...
                    Screen('Flip', obj.winPtr, vbl+time-obj.dis.hfd);
                iFlip = iFlip + 1;
            end
        end
    end
end

